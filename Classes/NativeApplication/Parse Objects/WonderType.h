//
//  WonderType.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 09/02/14.
//
//

#import <Parse/Parse.h>

@interface WonderType : PFObject<PFSubclassing>

+ (NSString *)parseClassName;

@property (retain) NSString *name;

@end
