//
//  Wonder.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 31/01/14.
//
//

#import "Wuser.h"
#import <Parse/PFObject+Subclass.h>

@implementation Wuser

@dynamic name;
@dynamic usernameNormalized;
@dynamic email;
@dynamic numComments;
@dynamic numFollowers;
@dynamic numLikes;
@dynamic numVisits;
@dynamic isEmailPublic;
@dynamic timeline_file;
@dynamic picture_file;
@dynamic timeline_url;
@dynamic picture_url;
@dynamic socialProfile;
@dynamic usersFollowed;

/*
 @property (retain) NSString *name;
 @property (retain) NSNumber *numComments;
 @property (retain) NSNumber *numLikes;
 @property (retain) NSNumber *numVisits;
 @property (retain) NSNumber *numFollowers;
 
 @property BOOL isEmailPublic;
 
 @property (retain) NSString *timeline_url;
 @property (retain) NSString *picture_url;
 
 @property (retain) PFFile *timeline_file;
 @property (retain) PFFile *picture_file;
 
 @property (retain) NSNumber *socialProfile;
*/

+ (NSString *)parseClassName {
    return @"_User";
}

@end
