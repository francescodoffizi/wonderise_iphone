//
//  Wonder.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 31/01/14.
//
//

#import <Parse/Parse.h>

@interface Wuser : PFUser<PFSubclassing>

+ (NSString *)parseClassName;

//@property (retain) NSString *objectId;

@property (retain) NSString *name;
@property (retain) NSString *usernameNormalized;
@property (retain) NSNumber *numComments;
@property (retain) NSNumber *numLikes;
@property (retain) NSNumber *numVisits;
@property (retain) NSNumber *numFollowers;

@property BOOL isEmailPublic;

@property (retain) NSString *timeline_url;
@property (retain) NSString *picture_url;

@property (retain) PFFile *timeline_file;
@property (retain) PFFile *picture_file;

@property (retain) NSNumber *socialProfile;

@property (retain) PFRelation *usersFollowed;
/*
public static final String FIELD_ID = "objectId";
public static final String FIELD_DESCRIPTION = "description";
public static final String FIELD_NOTE = "note";
public static final String FIELD_LOCATION = "location";
public static final String FIELD_USER = "user";
public static final String FIELD_CATEGORY = "type";
public static final String FIELD_MARKERIMAGE = "image";
public static final String FIELD_META = "meta";
public static final String FIELD_METAPOSITION = "metaPosition";
public static final String FIELD_METAROTATION = "metaRotation";
public static final String FIELD_METASCALE = "metaScale";
public static final String FIELD_ISPUBLISHED = "isPublished";
*/

@end
