//
//  Meta.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 09/02/14.
//
//

#import <Parse/Parse.h>

@interface Meta : PFObject<PFSubclassing>

+ (NSString *)parseClassName;

@property (retain) NSString *name;
@property (retain) NSString *nameNormalized;
@property (retain) PFFile *thumbnail;
@property (retain) PFFile *contentObjiOS;

@property (retain) PFUser *user;

@property BOOL isPublished;

@property (retain) NSNumber *numLikes;
@property (retain) NSNumber *numVisits;


@end
