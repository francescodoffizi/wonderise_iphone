//
//  WonderType.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 09/02/14.
//
//

#import "WonderType.h"
#import <Parse/PFObject+Subclass.h>

@implementation WonderType

@dynamic name;

+ (NSString *)parseClassName {
    return @"MarkerType";
}

@end
