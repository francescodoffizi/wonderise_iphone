//
//  WonderComment.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 02/05/14.
//
//

#import <Parse/Parse.h>
#import "Wuser.h"
#import "Wonder.h"

@interface WonderUserRelation : PFObject<PFSubclassing>

+ (NSString *)parseClassName;

@property (retain) Wuser *user;
@property (retain) Wonder *wonder;
@property BOOL likesTheWonder;
@property NSNumber *numVisits;

@end
