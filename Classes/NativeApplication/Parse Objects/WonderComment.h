//
//  WonderComment.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 02/05/14.
//
//

#import <Parse/Parse.h>
#import "Wuser.h"
#import "Wonder.h"

@interface WonderComment : PFObject<PFSubclassing>

+ (NSString *)parseClassName;

@property (retain) Wuser *user;
@property (retain) Wonder *wonder;
@property (retain) NSString *text;
@end
