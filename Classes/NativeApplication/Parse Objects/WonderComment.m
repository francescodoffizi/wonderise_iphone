//
//  WonderComment.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 02/05/14.
//
//

#import "WonderComment.h"
#import <Parse/PFObject+Subclass.h>

@implementation WonderComment

@dynamic wonder;
@dynamic user;
@dynamic text;

+ (NSString *)parseClassName {
    return @"WonderComment";
}
@end
