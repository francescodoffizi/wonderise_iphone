//
//  Meta.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 09/02/14.
//
//

#import "Meta.h"
#import <Parse/PFObject+Subclass.h>

@implementation Meta

@dynamic name;
@dynamic nameNormalized;
@dynamic thumbnail;
@dynamic user;
@dynamic contentObjiOS;
@dynamic numLikes;
@dynamic numVisits;
@dynamic isPublished;

+ (NSString *)parseClassName {
    return @"Meta";
}

@end
