//
//  Wonder.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 31/01/14.
//
//

#import "Wonder.h"
#import <Parse/PFObject+Subclass.h>

@implementation Wonder

@dynamic description;
@dynamic location;
@dynamic note;
@dynamic user;
@dynamic meta;
@dynamic type;
@dynamic metaPosition;
@dynamic metaRotation;
@dynamic metaScale;
@dynamic isPublished;

@dynamic numComments;
@dynamic numLikes;
@dynamic numVisits;

@dynamic image;

@dynamic city;
@dynamic country;

@dynamic isTargetReady;
@dynamic targetID;


@dynamic usersFollowed;

+ (NSString *)parseClassName {
    return @"Marker";
}

@end
