//
//  Wonder.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 31/01/14.
//
//

#import <Parse/Parse.h>

@interface Wonder : PFObject<PFSubclassing>

+ (NSString *)parseClassName;

//@property (retain) NSString *objectId;

@property (retain) NSString *description;
@property (retain) PFFile *image;
@property (retain) PFGeoPoint *location;

@property (retain) NSString *note;

@property (retain) PFUser *user;

@property (retain) PFObject *meta;
@property (retain) PFObject *type;

@property (retain) NSArray *metaPosition;
@property (retain) NSArray *metaRotation;
@property (retain) NSNumber *metaScale;

@property (retain) NSNumber *numLikes;
@property (retain) NSNumber *numVisits;
@property (retain) NSNumber *numComments;

@property (retain) PFRelation *usersFollowed;

@property (retain) NSString *city;
@property (retain) NSString *country;

@property (retain) NSString *targetID;


@property BOOL isPublished;
@property BOOL isTargetReady;

/*
public static final String FIELD_ID = "objectId";
public static final String FIELD_DESCRIPTION = "description";
public static final String FIELD_NOTE = "note";
public static final String FIELD_LOCATION = "location";
public static final String FIELD_USER = "user";
public static final String FIELD_CATEGORY = "type";
public static final String FIELD_MARKERIMAGE = "image";
public static final String FIELD_META = "meta";
public static final String FIELD_METAPOSITION = "metaPosition";
public static final String FIELD_METAROTATION = "metaRotation";
public static final String FIELD_METASCALE = "metaScale";
public static final String FIELD_ISPUBLISHED = "isPublished";
*/

@end
