//
//  WonderComment.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 02/05/14.
//
//

#import "WonderUserRelation.h"
#import <Parse/PFObject+Subclass.h>

@implementation WonderUserRelation

@dynamic wonder;
@dynamic user;
@dynamic likesTheWonder;
@dynamic numVisits;

+ (NSString *)parseClassName {
    return @"WonderUserRelation";
}
@end
