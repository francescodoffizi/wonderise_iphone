//
//  MetaFilterViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 19/04/14.
//
//

#import "MetaFilterViewController.h"
#import "MetaForCreateViewController.h"

@interface MetaFilterViewController ()

@end

@implementation MetaFilterViewController
@synthesize metaName,publicGallery,privateGallery,titleSearch,userSearch,metaForCreate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    isFilterVisible = false;
    /*
     UISwipeGestureRecognizer * swipeUp=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(slideIn:)];
     swipeUp.direction=UISwipeGestureRecognizerDirectionUp;
     [swipeUp setCancelsTouchesInView:YES];
     [self.view addGestureRecognizer:swipeUp];
     
     UISwipeGestureRecognizer * swipeDown=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(slideIn:)];
     swipeDown.direction=UISwipeGestureRecognizerDirectionDown;
     [swipeDown setCancelsTouchesInView:YES];
     [self.view addGestureRecognizer:swipeDown];
     */
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(IBAction)slideFilter:(id)sender {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    if (isFilterVisible)
        [self moveFilterOut];
    else
        [self moveFilterIn];
    [UIView commitAnimations];
}


- (void)slideIn:(UIGestureRecognizer *)sender {
    [self moveFilterIn];
}

- (void)slideOut:(UIGestureRecognizer *)sender {
    [self moveFilterOut];
}


-(void)moveFilterIn {
    if (!isFilterVisible) {
        isFilterVisible = true;
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y-132, self.view.frame.size.width, self.view.frame.size.height)];
    }
}
-(void)moveFilterOut {
    if (isFilterVisible) {
        isFilterVisible = false;
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y+132, self.view.frame.size.width, self.view.frame.size.height)];
    }
        [metaForCreate filterMetas:self];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //hide the keyboard
    [textField resignFirstResponder];
    
    //return NO or YES, it doesn't matter
    return YES;
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -215; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}


-(IBAction)switchGallery:(id)sender {
    if ([sender tag]==0) {
        [privateGallery setOn:![sender isOn]];
    }
    else {
        [publicGallery setOn:![sender isOn]];
    }
}

-(IBAction)switchFiltering:(id)sender {
    if ([sender tag]==0) {
        [userSearch setOn:![sender isOn]];
    }
    else {
        [titleSearch setOn:![sender isOn]];
    }
}


@end
