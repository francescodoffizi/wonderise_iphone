//
//  DetailFilterViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 12/05/14.
//
//

#import <UIKit/UIKit.h>
#import "AKSSegmentedSliderControl.h"

@class MapFilterViewController;

@interface DetailFilterViewController : UIViewController <AKSSegmentedSliderControlDelegate> {
    int selectedValue;
    NSInteger *filterType;
    MapFilterViewController *mfvc;
    int currentFiltering;
}

@property (nonatomic, retain) MapFilterViewController *mfvc;
-(void)setupFilter:(int)type;
-(void)prepareSlider;
- (void)segmentedControlValueChanged:(id)sender;
@end
