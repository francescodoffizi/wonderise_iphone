//
//  MetaForCreateViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 18/04/14.
//
//

#import <UIKit/UIKit.h>
#import "MetaFilterViewController.h"
#import "MetaForCreateTableViewController.h"

@interface MetaForCreateViewController : UIViewController {    
    MetaFilterViewController *mfvc;
    MetaForCreateTableViewController *mfctvc;
}

@property (nonatomic, retain) NSMutableArray *metas;
@property (nonatomic, retain) MetaForCreateTableViewController *mfctvc;
-(IBAction)filterMenu:(id)sender;

-(void)filterMetas:(MetaFilterViewController*)mfvc;

@end
