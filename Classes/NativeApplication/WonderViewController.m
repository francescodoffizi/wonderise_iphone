//
//  WonderViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 23/04/14.
//
//

#import "WonderViewController.h"
#import "WonderComment.h"
#import "TNAppDelegate.h"
#import "UnityIOSBridge.h"
#import "EditWonderViewController.h"
#import "TWMessageBarManager.h"
#import "ProfileViewController.h"

@interface WonderViewController ()

@end

@implementation WonderViewController
@synthesize wonder;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    commentsArray = [[NSMutableArray alloc] init];
    [wonderImage setImageURL:[NSURL URLWithString:[(PFFile*)[wonder image] url]]];
    [editWonderBtn setAlpha:0.0f];
    [editWonderBtn setEnabled:NO];
    [[wonder user] fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if ([[[wonder user] username] isEqualToString:[[PFUser currentUser] username]]) {
            [editWonderBtn setAlpha:1.0f];
            [editWonderBtn setEnabled:YES];
        }
    }];
    
    /*
     Circular mask to profile image
     */
    
    pictureProfileImgView.layer.borderWidth=3.0;
    pictureProfileImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    pictureProfileImgView.layer.backgroundColor=[UIColor clearColor].CGColor;
    pictureProfileImgView.layer.cornerRadius=pictureProfileImgView.bounds.size.width/2;
    [pictureProfileImgView.layer setMasksToBounds:YES];
    [[wonder user] fetchIfNeeded];
    [wonderName setText:[(NSString*)[wonder note] uppercaseString]];
    [wonderDescription setText:[wonder objectForKey:@"description"]];
    [[wonder type] fetchIfNeeded];
    [wonderCategory setText:[(WonderType*)[wonder type] name]];
    
    NSString *dateString = [NSDateFormatter localizedStringFromDate:[wonder createdAt]
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:nil];
    
    [wonderPostDate setText:dateString];

    
    [commentsCount setText:[[wonder numComments] stringValue]];
    [viewsCount setText:[[wonder numVisits] stringValue]];
    [likeCount setText:[[wonder numLikes] stringValue]];
    
    [pictureProfileImgView setImageURL:[NSURL URLWithString:[(Wuser *)[wonder user] picture_url]]];
    [self checkWonderState:nil];
    [self registerForKeyboardNotifications];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"commentsEmbed"]) {
        commentsArray = [[NSMutableArray alloc] init];
        PFQuery *query = [[WonderComment query] retain];
        [query whereKey:@"wonder" equalTo:wonder];
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            for (WonderComment *comment in objects) {
                [commentsArray addObject:comment];
            }
            wctvc = [segue destinationViewController];
            [wctvc setComments:commentsArray];
            [wctvc setWonder:wonder];
            [wctvc setWvc:self];
            
            [[wctvc tableView] reloadData];
        }];
        /*Qui devo passare al controller i dati sui commenti e la paginazione*/
    }
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    if (!commentsArray)
        commentsArray = [[NSMutableArray alloc] init];
    //
    //[commentsContainer setFrame:CGRectMake(0, commentsContainer.frame.origin.y, 320, ([commentsArray count]+1)*75)];
    [[wctvc view] setFrame:CGRectMake(0, 0, 320, wctvc.tableView.contentSize.height)];
    [commentsContainer invalidateIntrinsicContentSize];

    /*[scrollView setFrame:CGRectMake(scrollView.frame.origin.x, scrollView.frame.origin.y, 320, wctvc.view.frame.size.height)];
     */
    [scrollView setScrollEnabled:YES];
    [scrollView setContentSize:CGSizeMake(320, 549+wctvc.tableView.contentSize.height)];

}

-(IBAction)showWonder:(id)sender {
    TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    UnityIOSBridge *unityBridge = [appDelegate unityBridge];
    Meta *meta = (Meta*)[wonder meta];
    [meta fetchIfNeeded];
    NSMutableDictionary *jsonDict = [[NSMutableDictionary alloc] init];
    [jsonDict setValue:[(PFFile*)[meta contentObjiOS] url] forKey:@"url"];
    [jsonDict setValue:[wonder targetID] forKey:@"targetID"];
    
    NSArray* positions = [wonder metaPosition];
    NSArray* rotations = [wonder metaRotation];
    NSNumber* scale = [wonder metaScale];

    [jsonDict setValue:[positions objectAtIndex:0] forKey:@"position_x"];
    [jsonDict setValue:[positions objectAtIndex:1] forKey:@"position_y"];
    [jsonDict setValue:[positions objectAtIndex:2] forKey:@"position_z"];

    [jsonDict setValue:[rotations objectAtIndex:0] forKey:@"rotation_x"];
    [jsonDict setValue:[rotations objectAtIndex:1] forKey:@"rotation_y"];
    [jsonDict setValue:[rotations objectAtIndex:2] forKey:@"rotation_z"];
    
    [jsonDict setValue:scale forKey:@"scale"];
    
    [jsonDict setValue:[meta objectId] forKey:@"metaID"];
    
#warning UnityPause
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict
                                                       options:0 error:&error];
    
    [wonder incrementKey:@"numVisits"];
    [wonder saveEventually:^(BOOL succeeded, NSError *error) {
        if (error) {
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore" description:@"Impossibile aggiornare le statistiche sulla tua visita al Wonder" type:TWMessageBarMessageTypeError];
        }
    }];
    
    [unityBridge loadVisualizationScene:[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding]];
    //[[self navigationController] pushViewController:[appDelegate unityVC] animated:YES];
    [[appDelegate unityVC] setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    [self presentViewController:[appDelegate unityVC] animated:YES completion:^{
        NSLog(@"Ho Finito");
        [appDelegate unityPause:NO];

    }];
}

-(IBAction)checkWonderState:(id)sender {
    if ([wonder isTargetReady]>0) {
        [wonderPlay setAlpha:1.0f];
        [notReadyWonder setAlpha:0.0f];
        [wonderPlay setUserInteractionEnabled:YES];
    }
    else {
        [wonderPlay setAlpha:0.0f];
        [notReadyWonder setAlpha:1.0f];
        [wonderPlay setUserInteractionEnabled:NO];
    }
}

-(IBAction)switchMetaWonder:(id)sender {
    if ([sender tag]==0) {
        [sender setTag:1];
        Meta *meta = (Meta*)[wonder meta];
        [meta fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            [wonderImage setImageURL:[NSURL URLWithString:[[meta thumbnail] url]]];
            [wonderPlay setAlpha:0.0f];
            [notReadyWonder setAlpha:0.0f];
            [wonderPlay setUserInteractionEnabled:NO];
        }];
    }
    else {
        [sender setTag:0];
        [wonderImage setImageURL:[NSURL URLWithString:[(PFFile*)[wonder image] url]]];
        [self checkWonderState:nil];
    }
}
-(void)updateReadyWonderStatus:(BOOL)status {
    if (status) {
        //wonder should be now ready!
        [self checkWonderState:self];
    }
}

-(IBAction)editWonder:(id)sender {
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    EditWonderViewController *evc  = [sb instantiateViewControllerWithIdentifier:@"editWonderViewController"];
    [evc setWonder:wonder];
    [[SlideNavigationController sharedInstance] pushViewController:evc animated:YES];
}


// Call this method somewhere in your view controller setup code.

- (void)registerForKeyboardNotifications

{
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
    
}



// Called when the UIKeyboardDidShowNotification is sent.

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
    
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    
    // Your app might not need or want this behavior.
    
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, commentsContainer.frame.origin) ) {
        CGRect showFrame = CGRectMake(0, scrollView.contentSize.height-75, 320, 75);
        [scrollView scrollRectToVisible:showFrame animated:YES];
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}


- (void)textViewDidBeginEditing:(UITextView *)textField

{
    
    activeField = textField;
}



- (void)textViewDidEndEditing:(UITextView *)textField

{
    
    activeField = nil;
    
}
-(void)reloadWonderComments {
    commentsArray = [[NSMutableArray alloc] init];
    PFQuery *query = [[WonderComment query] retain];
    [query whereKey:@"wonder" equalTo:wonder];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        for (WonderComment *comment in objects) {
            [commentsArray addObject:comment];
        }
        
        [wctvc setComments:commentsArray];
        [[wctvc tableView] reloadData];
        
        [[wctvc view] setFrame:CGRectMake(0, 0, 320, wctvc.tableView.contentSize.height)];
        CGRect frameold = commentsContainer.frame;
        [commentsContainer setFrame:CGRectMake(0, commentsContainer.frame.origin.y, 320, wctvc.tableView.contentSize.height)];
        CGRect framenew = commentsContainer.frame;
        [scrollView setContentSize:CGSizeMake(320, 549+(wctvc.tableView.contentSize.height))];
        [commentsContainer invalidateIntrinsicContentSize];
        
    }];
}

-(IBAction)likeWonder:(id)sender {
    if ([sender tag]==0) {
        //add like
        [sender setTag:1];
        
    }
    else {
        //remove like
        [sender setTag:0];
    }
}

-(IBAction)showWonderOwner:(id)sender {
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *upvc  = [sb instantiateViewControllerWithIdentifier:@"profileView"];
    [[wonder user] fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            [upvc setUserToShow:(Wuser*)[wonder user]];
            [[SlideNavigationController sharedInstance] pushViewController:upvc animated:YES];
        }
        else {
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore" description:error.localizedDescription type:TWMessageBarMessageTypeError];
        }
    }];
}


@end
