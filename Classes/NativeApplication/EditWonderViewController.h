//
//  EditWonderViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 29/04/14.
//
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "Wonder.h"
#import "WonderType.h"
#import "Meta.h"

@interface EditWonderViewController : UIViewController {
    IBOutlet AsyncImageView *wonderImageView;
    IBOutlet UITextField *titleField;
    IBOutlet UITextView *descriptionField;
    IBOutlet UIButton *categoryBtn;
    IBOutlet UITextField *categoryTextField;
    
    IBOutlet UIImageView *okTitle;
    IBOutlet UIImageView *okDescription;
    IBOutlet UIImageView *okCategory;
    IBOutlet UISwitch *publicSwitch;
    
    
    BOOL isCategoryMenuVisible;
    BOOL isKeyboardShown;
    BOOL haveCategoryBeenLoaded;
    IBOutlet UIView *controllerViewContainer;
    
    UIView *activeField;
    
    NSNumber *selectedCategory;
    
    WonderType *selectedWonderType;
    NSArray *wonderTypes;
    
    NSMutableDictionary *unityData;

    IBOutlet UIButton *doneBtn;
    IBOutlet UIScrollView *scrollView;
    
    Wonder *wonder;
}

@property (nonatomic, retain) NSMutableDictionary *unityData;
@property (nonatomic, retain) Wonder *wonder;

-(IBAction)showCategoryMenu:(id)sender;
-(void)saveWonder;
-(IBAction)checkDataAndsaveWonder:(id)sender;
-(void)animateTextFieldUp:(BOOL)up;
-(void)selectCategory:(NSInteger)selected;
- (void)registerForKeyboardNotifications;
-(void)checkAndUpdateFieldsAlert;

@end
