//
//  MapFilterViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 12/05/14.
//
//

#import "MapFilterViewController.h"

@interface MapFilterViewController ()

@end

@implementation MapFilterViewController
@synthesize mvc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(IBAction)showHideFilter:(id)sender {
    [mvc showHideFilters:sender andVC:self];
}

-(IBAction)showDetailFilter:(id)sender {
    if ([sender tag]>1 && [sender tag]<4) {
        for (UIView* subview in [dfvc.view subviews]) {
            [subview removeFromSuperview];
        }
        activeButton = sender;
        if ([activeButton isSelected]) {
            [self updateFilter:[sender tag] and:-1];
        }
        else {
            [self updateFilter:[sender tag] and:1];
        }
    }
    else {
        if (sender == activeButton) {
            //disable filter
            //[self updateFilter:[sender tag] and:-1];
        }
        else {
            activeButton = sender;
            [dfvc setupFilter:[sender tag]];
        }
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    //check segue identifier if segueaction is added
    dfvc = [segue destinationViewController];
    [dfvc setMfvc:self];
}

-(void)updateFilter:(int)index and:(int)value {
    [mvc updateFilter:index and:value];
    if (value>=0) {
        [activeButton setSelected:YES];
    }
    else {
        [activeButton setSelected:NO];
    }
}
@end
