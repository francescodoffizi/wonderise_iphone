//
//  UserProfileEditViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 23/05/14.
//
//

#import "UserProfileEditViewController.h"
#import <Parse/Parse.h>
#import "TWMessageBarManager.h"
#import "Wuser.h"

@interface UserProfileEditViewController ()

@end

@implementation UserProfileEditViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    imageWorked = -1;
    Wuser *user = (Wuser*)[PFUser currentUser];
    [timelineImageView setImageURL:[NSURL URLWithString:[user timeline_url]]];
    [profileImageView setImageURL:[NSURL URLWithString:[user picture_url]]];
    
    profileImageView.layer.borderWidth=3.0;
    profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    profileImageView.layer.backgroundColor=[UIColor clearColor].CGColor;
    profileImageView.layer.cornerRadius=profileImageView.bounds.size.width/2;
    [profileImageView.layer setMasksToBounds:YES];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(IBAction)editProfileImage:(id)sender {
    imageWorked = 0;
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate=self;
    [picker setSourceType:(UIImagePickerControllerSourceTypePhotoLibrary)];
    [[self navigationController] presentViewController:picker animated:YES completion:Nil];
}
-(IBAction)editTimelineImage:(id)sender {
    imageWorked = 1;
    
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate=self;
    [picker setSourceType:(UIImagePickerControllerSourceTypePhotoLibrary)];
    [[self navigationController] presentViewController:picker animated:YES completion:Nil];
    
}
-(void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage {
    //Devo salvare la nuova immagine
    NSData *imageData = UIImagePNGRepresentation(editedImage);
    PFFile *imageFile = [PFFile fileWithData:imageData];
    
    [imageFile saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (succeeded) {
            if (imageWorked==0) {
                Wuser *currentUser = (Wuser*)[PFUser currentUser];
                [currentUser setPicture_file:imageFile];
                [currentUser setPicture_url:[imageFile url]];
                [currentUser saveEventually:^(BOOL succeeded, NSError *error) {
                    if (succeeded) {
                        
                        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Conferma" description:@"Salvataggio dell'immagine del profilo completata..." type:TWMessageBarMessageTypeSuccess];
                    }
                    else {
                        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore" description:@"Problema durante il salvataggio dell'immagine del profilo, controlla la connettività e riprova..." type:TWMessageBarMessageTypeError];
                    }
                }];
            } // profilo
            else if (imageWorked==1) {
                Wuser *currentUser = (Wuser*)[PFUser currentUser];
                [currentUser setTimeline_file:imageFile];
                [currentUser setTimeline_url:[imageFile url]];
                [currentUser saveEventually:^(BOOL succeeded, NSError *error) {
                    if (succeeded) {
                        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Conferma" description:@"Salvataggio dell'immagine della timeline completata..." type:TWMessageBarMessageTypeSuccess];
                    }
                    else {
                        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore" description:@"Problema durante il salvataggio dell'immagine della timeline, controlla la connettività e riprova..." type:TWMessageBarMessageTypeError];
                    }
                }];
            } //timeline
        }
        else {
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore" description:@"Problema durante il salvataggio dell'immagine del timeline, controlla la connettività e riprova..." type:TWMessageBarMessageTypeError];
        }
    }];
    
    [self dismissViewControllerAnimated:YES completion:^{
        // Do nothing
    }];
    
}

-(void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController {
    //Non devo fare niente... ha annullato
    [self dismissViewControllerAnimated:YES completion:^{
        // Do nothing
    }];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [picker dismissViewControllerAnimated:NO completion:nil];
    int height=141;
    if (imageWorked==0)
        height=320;
    VPImageCropperViewController *imgCropperVC = [[VPImageCropperViewController alloc] initWithImage:info[UIImagePickerControllerOriginalImage] cropFrame:CGRectMake(0, 100.0f, 320, height) limitScaleRatio:3.0];
    imgCropperVC.delegate = self;
    [self presentViewController:imgCropperVC animated:YES completion:^{
        // TO DO
    }];
}

-(IBAction)showPreview:(id)sender {
    int movement = 75;
    int rotation = 180;
    if ([sender tag]==0) {
        movement = 0;
        [sender setTag:1];
    }
    else  {
        [sender setTag:0];
        movement = -185;
        rotation = 0;
    }
    [UIView beginAnimations: @"animatePreview" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: 0.3f];
    editingArrow.transform = CGAffineTransformMakeRotation(rotation * M_PI/180);
//    editingView.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
    [UIView animateWithDuration:0.3
                     animations:^{
                         topConstraint.constant = movement;
                         [[self view] layoutIfNeeded];
                     }];
    
}

@end
