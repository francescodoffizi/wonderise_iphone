//
//  MetaWonderCell.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 07/02/14.
//
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface MetaWonderCell : UITableViewCell {
    IBOutlet UILabel *objectType;
    IBOutlet UILabel *date;
    IBOutlet UILabel *location;
    
    IBOutlet UILabel *likesNumber;
    IBOutlet UILabel *commentsNumber;
    IBOutlet UILabel *viewsNumber;

    IBOutlet UILabel *followersNumber;

    
    IBOutlet AsyncImageView *objectImageView;
    
    IBOutlet UIImageView *cellBackground;
    /*
     art
     eco
     info
     play
     tourism
     */
    id usefulObject;
    int cellType;
}

@property (nonatomic, retain) IBOutlet UILabel *objectType;
@property (nonatomic, retain) IBOutlet UILabel *date;
@property (nonatomic, retain) IBOutlet UILabel *location;

@property (nonatomic, retain) IBOutlet UILabel *likesNumber;
@property (nonatomic, retain) IBOutlet UILabel *commentsNumber;
@property (nonatomic, retain) IBOutlet UILabel *viewsNumber;
@property (nonatomic, retain) IBOutlet UILabel *followersNumber;
@property (nonatomic, retain) id usefulObject;
@property (nonatomic) int cellType;
@property (nonatomic, retain)    IBOutlet UIImageView *cellBackground;

@property (nonatomic, retain) IBOutlet AsyncImageView *objectImageView;

-(void)setElementType:(NSString*)type;

@end
