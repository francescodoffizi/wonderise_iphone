//
//  UserRegistrationViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 03/02/14.
//
//

#import "UserLoginSignupViewController.h"
#import "TNAppDelegate.h"

#import "MBProgressHUD.h"
#import "TWMessageBarManager.h"

@interface UserLoginSignupViewController ()

@end

@implementation UserLoginSignupViewController

@synthesize social;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        /*username.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        email.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
        password.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
         */
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)doLoginSignupViaSocialNetwork:(id)sender {
    social = [sender tag];
    TNAppDelegate* appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate userLoginSignupViaSocialNetwork:self];
}

-(IBAction)doLocalSignup:(id)sender {
    Wuser *user = [[Wuser alloc] init];
    [user setUsername:username.text];
    [user setPassword:password.text];
    [user setEmail:email.text];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[[[[UIApplication sharedApplication] delegate] window] rootViewController]view] animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Signup in corso...";
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        [hud hide:YES];
        [hud release];
        if (succeeded) {
            [self dismissViewControllerAnimated:YES completion:^{
                
            }];
        }
        else {
            NSString *errorString = [error userInfo][@"error"];
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                           description:errorString
                                                                  type:TWMessageBarMessageTypeError callback:^{
                                                                      NSLog(@"Message bar tapped!");
                                                                  }];
            
        }
    }];
}

-(IBAction)doLocalLogin:(id)sender {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[[[[UIApplication sharedApplication] delegate] window] rootViewController]view] animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Login in corso...";

    [PFUser logInWithUsernameInBackground:username.text password:password.text block:^(PFUser *user, NSError *error) {
        [hud hide:YES];
        [hud release];
        if (!error) {
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                           description:@"Verifica i dati d'accesso."
                                                                  type:TWMessageBarMessageTypeError callback:^{
                                                                      NSLog(@"Message bar tapped!");
                                                                  }];
        }
        [self dismissViewControllerAnimated:YES completion:^{
            
        }];
    }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField:textField up:YES];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField:textField up:NO];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //hide the keyboard
    [textField resignFirstResponder];
    
    //return NO or YES, it doesn't matter
    return YES;
}

-(void)animateTextField:(UITextField*)textField up:(BOOL)up
{
    const int movementDistance = -128; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

@end
