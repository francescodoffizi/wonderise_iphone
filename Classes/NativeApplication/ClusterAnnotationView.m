//
//  ClusterAnnotationView.m
//  CCHMapClusterController Example iOS
//
//  Created by Hoefele, Claus(choefele) on 09.01.14.
//  Copyright (c) 2014 Claus Höfele. All rights reserved.
//

// Based on https://github.com/thoughtbot/TBAnnotationClustering/blob/master/TBAnnotationClustering/TBClusterAnnotationView.m by Theodore Calmes

#import "ClusterAnnotationView.h"
#import "CCHMapClusterAnnotation.h"
#import "AsyncImageView.h"
#import "Wonder.h"
#import "WonderAnnotation.h"

#import "RNBlurModalView.h"

#import "CircleMenuViewController.h"
#import "WonderViewController.h"

#import "SlideNavigationController.h"

#import "WonderToViewViewController.h"

#import "MapViewController.h"


// Button Size
#define kKYButtonInMiniSize 16.f
#define kKYButtonInSmallSize 32.f
#define kKYButtonInNormalSize 64.f
// Number of buttons around the circle menu
#define kKYCCircleMenuButtonsCount 6
// Circle Menu
// Basic constants
#define kKYCircleMenuSize 280.f
#define kKYCircleMenuButtonSize kKYButtonInNormalSize
#define kKYCircleMenuCenterButtonSize kKYButtonInNormalSize
// Image
#define kKYICircleMenuCenterButton @"KYICircleMenuCenterButton.png"
#define kKYICircleMenuCenterButtonBackground @"KYICircleMenuCenterButtonBackground.png"
#define kKYICircleMenuButtonImageNameFormat @"KYICircleMenuButton%.2d.png" // %.2d: 1 - 6



static inline CGPoint TBRectCenter(CGRect rect)
{
    return CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
}

static inline CGRect TBCenterRectRounded(CGRect rect, CGPoint center)
{
    CGRect r = CGRectMake(roundf(center.x - rect.size.width/2.0),
                          roundf(center.y - rect.size.height/2.0),
                          roundf(rect.size.width),
                          roundf(rect.size.height));
    return r;
}

static CGFloat const TBScaleFactorAlpha = 0.3;
static CGFloat const TBScaleFactorBeta = 0.4;

static inline CGFloat TBScaledValueForValue(CGFloat value)
{
    return 1.0 / (1.0 + expf(-1 * TBScaleFactorAlpha * powf(value, TBScaleFactorBeta)));
}

@interface ClusterAnnotationView ()

@property (strong, nonatomic) UILabel *countLabel;

@end

@implementation ClusterAnnotationView
@synthesize wonderAnnotations, mvc;

- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self setupLabel];
        [self setCount:1];
    }
    return self;
}

- (void)setupLabel
{
    _countLabel = [[UILabel alloc] initWithFrame:self.frame];
    _countLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _countLabel.textAlignment = NSTextAlignmentCenter;
    _countLabel.backgroundColor = [UIColor clearColor];
    _countLabel.textColor = [UIColor blackColor];
    _countLabel.textAlignment = NSTextAlignmentCenter;
    _countLabel.adjustsFontSizeToFitWidth = YES;
    _countLabel.numberOfLines = 1;
    _countLabel.font = [UIFont boldSystemFontOfSize:10];
    _countLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    [self addSubview:_countLabel];
}

- (void)removeLabel {
    [_countLabel setAlpha:0.0f];
    [self removeImage];
}

- (void)removeImage {
    [_imageView removeFromSuperview];
    [_defaultImageView removeFromSuperview];
}

- (void)setupDefaultImageAndCounter {
    [self removeImage];
    
    AsyncImageView *aiv = [[AsyncImageView alloc] init];
    _defaultImageView = aiv;
    [self addSubview:aiv];
    [aiv setImage:[UIImage imageNamed:@"cluster_icon.png"]];
    
    [_countLabel setAlpha:1.0f];
    [self bringSubviewToFront:_countLabel];
    if (_btn)
        [_btn removeFromSuperview];
    [self addActionGrabber:1];
}


- (void)setupImagePinWithUrl:(NSURL*)url {
    [self removeLabel];
    AsyncImageView *aiv = [[AsyncImageView alloc] init];
    [aiv setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _imageView = aiv;
    [self addSubview:_imageView];
    [aiv setImageURL:url];
    [self addActionGrabber:0];
}

- (void)setupImagePinWithImage:(UIImage*)image {
    [self removeLabel];
    UIImageView *aiv = [[[UIImageView alloc] initWithImage:image] retain];
    [aiv setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    NSLog(@"%f,%f",self.frame.size.width, self.frame.size.height);
    _imageView = aiv;
    [self addSubview:_imageView];
    [self addActionGrabber:0];
}

- (void)setCount:(NSUInteger)count
{
    _count = count;
    
    CGPoint oldCenter = self.center;
    CGRect newBounds;
    if (count > 1)
        newBounds = CGRectMake(0, 0, 44 * TBScaledValueForValue(count), 44 * TBScaledValueForValue(count));
    else
        newBounds = CGRectMake(0, 0, 38,38);
    self.frame = TBCenterRectRounded(newBounds, oldCenter);
    self.center = oldCenter;
    
    self.countLabel.text = [@(count) stringValue];
    if (_count>1) {
        [self addSubview:_countLabel];
        [self setupDefaultImageAndCounter];
        if (_defaultImageView)
            [_defaultImageView setFrame:newBounds];
    }
    [self setNeedsDisplay];
}

-(void)addActionGrabber:(int)type {
    if (_btn)
        [_btn removeFromSuperview];
    _btn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    [_btn setFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    if (type==0) {
    [_btn addTarget:self
             action:@selector(showWonder:)
   forControlEvents:UIControlEventTouchUpInside];
    }
    else if (type==1) {
        [_btn addTarget:self
                 action:@selector(moreClusterDetails:)
       forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btn];
}

-(void)moreClusterDetails:(id)sender {
    NSMutableArray *wonders = [[NSMutableArray alloc] init];
    for (WonderAnnotation *wonderAnnotation in wonderAnnotations) {
        [wonders addObject:(Wonder*)wonderAnnotation.wonder];
    }
    
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WonderToViewViewController *wtvvc = [sb instantiateViewControllerWithIdentifier:@"wonderToViewViewController"];
    [wtvvc setTitle:@"Scegli il Wonder"];
    [wtvvc setWonders:wonders];
    [[SlideNavigationController sharedInstance] pushViewController:wtvvc animated:YES];
}

-(void)showWonder:(id)sender {

    /*Qui mostriamo il Wonder*/
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    NSMutableArray *wonders = [[NSMutableArray alloc] init];
    for (WonderAnnotation *wonderAnnotation in wonderAnnotations) {
        [wonders addObject:(Wonder*)wonderAnnotation.wonder];
    }
    if ([wonders count]>1) {
        WonderViewController *wvc  = [sb instantiateViewControllerWithIdentifier:@"wonderViewController"];
        [wvc setWonder:[(Wonder*)[wonders objectAtIndex:0] retain]];
        [[SlideNavigationController sharedInstance] pushViewController:wvc animated:YES];
    }
    else if ([wonders count]>0){
        UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        smivc = [[sb instantiateViewControllerWithIdentifier:@"slidingMetaInfoViewController"] retain];
        [smivc setWonderAndSetup:[(Wonder*)[wonders objectAtIndex:0] retain]];
        [mvc showSlidingMetaVC:smivc and:YES];
    }
}

/*- (void)drawRect:(CGRect)rect
 {
 CGContextRef context = UIGraphicsGetCurrentContext();
 
 CGContextSetAllowsAntialiasing(context, true);
 
 UIColor *outerCircleStrokeColor = [UIColor colorWithWhite:0 alpha:0.25];
 UIColor *innerCircleStrokeColor = [UIColor whiteColor];
 UIColor *innerCircleFillColor = [UIColor colorWithRed:(255.0 / 255.0) green:(95 / 255.0) blue:(42 / 255.0) alpha:1.0];
 
 CGRect circleFrame = CGRectInset(rect, 4, 4);
 
 [outerCircleStrokeColor setStroke];
 CGContextSetLineWidth(context, 5.0);
 CGContextStrokeEllipseInRect(context, circleFrame);
 
 [innerCircleStrokeColor setStroke];
 CGContextSetLineWidth(context, 4);
 CGContextStrokeEllipseInRect(context, circleFrame);
 
 [innerCircleFillColor setFill];
 CGContextFillEllipseInRect(context, circleFrame);
 }*/

@end
