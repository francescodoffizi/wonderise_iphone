//
//  UnityIOSBridge.m
//  Unity-iPhone
//
//  Created by Francesco D'Offizi on 07/02/14.
//
//

#import "UnityIOSBridge.h"
#import "SlideNavigationController.h"
#import "Wonder.h"
#import "MBProgressHUD.h"

#import <MapKit/MapKit.h>
#import "TNAppDelegate.h"


#import "TWMessageBarManager.h"

#import "EditWonderViewController.h"

void registerForRemoteNotifications()
{
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
}


void setWonderCreateSessionData(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float scale, char *path)
{
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    EditWonderViewController *evc  = [sb instantiateViewControllerWithIdentifier:@"editWonderViewController"];
    NSMutableDictionary *wonderData = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                       [NSNumber numberWithFloat:posX], @"posX",
                                       [NSNumber numberWithFloat:posY], @"posY",
                                       [NSNumber numberWithFloat:posZ], @"posZ",
                                       [NSNumber numberWithFloat:rotX], @"rotX",
                                       [NSNumber numberWithFloat:rotY], @"rotY",
                                       [NSNumber numberWithFloat:rotZ], @"rotZ",
                                       [NSNumber numberWithFloat:scale], @"scale",
                                       [NSString stringWithCString:(char*)path encoding:NSUTF8StringEncoding], @"path",
                                       nil];
    [evc setUnityData:wonderData];
    [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:^{
        [[SlideNavigationController sharedInstance] pushViewController:evc animated:YES];
    }];
}


void closeUnityActivity(BOOL param) {
    if (param) {
        //ERROR
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                       description:@"Qualcosa è andato storto nell'attività Unity."
                                                              type:TWMessageBarMessageTypeError callback:^{
                                                                  NSLog(@"Message bar tapped!");
                                                              }];
    }
    else {
        //ALLOK
        [[SlideNavigationController sharedInstance] dismissViewControllerAnimated:YES completion:nil];
        TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        
#warning UnityPause
        [appDelegate unityPause:YES];
    }
}
void messageFromUnity(char *message) {
    NSLog(@"Messaggio da Unity");
}

@implementation UnityIOSBridge
@synthesize metaToSave;

-(void)loadCreationScene:(NSString *)dataToSend
{
    UnitySendMessage("WonderiseSceneManager", "LoadCreateScene", [dataToSend UTF8String]);
}

-(void)loadVisualizationScene:(NSString *)dataToSend
{
    UnitySendMessage("WonderiseSceneManager", "LoadViewScene", [dataToSend UTF8String]);
}

@end