//
//  FavouriteMeta.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 22/06/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Appconfig : NSManagedObject

@property (nonatomic, retain) NSString * objectId;
@property (nonatomic) BOOL tutorial_run;

@end
