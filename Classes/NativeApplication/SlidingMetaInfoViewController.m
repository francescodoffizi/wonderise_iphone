//
//  SlidingMetaInfoViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 19/05/14.
//
//

#import "SlidingMetaInfoViewController.h"
#import "TNAppDelegate.h"
#import "WonderViewController.h"

@interface SlidingMetaInfoViewController ()

@end

@implementation SlidingMetaInfoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[wonderToShow meta] fetchIfNeeded];
    Meta* meta = (Meta*)[wonderToShow meta];
    [self setImageWithUrl:[[meta thumbnail] url] wonderName:[wonderToShow note] userName:[(Wuser*)[PFUser currentUser] name] andDesc:[wonderToShow objectForKey:@"description"]];
    [wonderTypeImgView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"wonderinfo_background_%@",[(NSString*)[(WonderType*)[wonderToShow objectForKey:@"type"] name] lowercaseString]]]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)setImageWithUrl:(NSString*)imageUrl wonderName:(NSString*)name userName:(NSString*)user andDesc:(NSString*)description {
    [metaImageView setImageURL:[NSURL URLWithString:imageUrl]];
    [userName setText:user];
    [wonderDescription setText:description];
    [wonderName setText:name];
    
    [wonderLike setText:[NSString stringWithFormat:@"%d",[[wonderToShow numLikes]intValue]]];
    [wonderComments setText:[NSString stringWithFormat:@"%d",[[wonderToShow numComments]intValue]]];
    [wonderViews setText:[NSString stringWithFormat:@"%d",[[wonderToShow numVisits]intValue]]];
    
}

-(void)setWonderAndSetup:(Wonder*)wonder {
    wonderToShow = wonder;
}

-(IBAction)showWonder:(id)sender {
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    WonderViewController *wvc  = [sb instantiateViewControllerWithIdentifier:@"wonderViewController"];
    if (![wonderToShow isTargetReady]) {
        VWSApi *vwsApi = [[VWSApi alloc] init];
        [vwsApi getTargetInfo:wonderToShow respondingTo:wvc];
    }
    [wvc setWonder:wonderToShow];
    [[SlideNavigationController sharedInstance] pushViewController:wvc animated:YES];
}

@end
