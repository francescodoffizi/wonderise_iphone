//
//  AppDelegate.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 17/01/14.
//
//

#import <UIKit/UIKit.h>
#import "UnityAppController.h"
#import "UI/UnityView.h"
#import "UI/UnityViewControllerBase.h"

#import "MenuViewController.h"
#import "SlideNavigationController.h"

#import "Wonder.h"
#import "WonderType.h"
#import "WonderComment.h"
#import "Wuser.h"
#import "Meta.h"
#import "VWSApi.h"
#import "WonderUserRelation.h"

#import "UnityIOSBridge.h"


@interface TNAppDelegate : UnityAppController {
    UnityIOSBridge *unityBridge;
    VWSApi *vwsApi;
    BOOL isShowingLoadingAlert;
    
    BOOL islaunched;
    
}
@property (retain) UnityAppController *unityController;
@property (nonatomic, strong) VWSApi *vwsApi;
@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) SlideNavigationController *navigationController;
@property (nonatomic, strong) UnityIOSBridge *unityBridge;
- (void)userLoginSignupViaSocialNetwork:(id)sender;

@property (nonatomic) BOOL isShowingLoadingAlert;


@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
