//
//  SlidingMetaInfoViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 19/05/14.
//
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "Wonder.h"

@interface SlidingMetaInfoViewController : UIViewController {
    IBOutlet AsyncImageView *metaImageView;
    IBOutlet UILabel *wonderName;
    IBOutlet UILabel *userName;
    IBOutlet UILabel *wonderDescription;
    Wonder *wonderToShow;
    IBOutlet UIImageView *wonderTypeImgView;
    
    IBOutlet UILabel *wonderLike;
    IBOutlet UILabel *wonderComments;
    IBOutlet UILabel *wonderViews;
    
}

-(void)setImageWithUrl:(NSString*)imageUrl wonderName:(NSString*)name userName:(NSString*)user andDesc:(NSString*)description;
-(void)setWonderAndSetup:(Wonder*)wonder;

-(IBAction)showWonder:(id)sender;

@end
