//
//  AppDelegate.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 17/01/14.
//
//

#import "TNAppDelegate.h"

#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

#import "UserLoginSignupViewController.h"

#import "TWMessageBarManager.h"

#import <CoreData/CoreData.h>


@implementation TNAppDelegate
@synthesize window, navigationController, unityBridge;
@synthesize vwsApi, isShowingLoadingAlert;

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [super application:application didFinishLaunchingWithOptions:launchOptions];
    islaunched = NO;
    unityBridge = [[UnityIOSBridge alloc] init];
    [Wonder registerSubclass];
    [Wuser registerSubclass];
    [Meta registerSubclass];
    [WonderType registerSubclass];
    [WonderComment registerSubclass];
    [WonderUserRelation registerSubclass];



    [Parse setApplicationId:@"aTt4oRgmba7zd16mjaj5XYlEhdGMH3c4oXJVvRRo"
                  clientKey:@"SRnIDdN0AsZrbrS9PH0I4btH8wsSG87BZTIkYLEJ"];
    
    [PFTwitterUtils initializeWithConsumerKey:@"BZtLizireGshRqRhQAtNl7HKo"
                               consumerSecret:@"QtahaRMEkM1cx3A5nHRIwId4qUPQNgIFeqfjBDYnijECxYQzbR"];
    
    [PFFacebookUtils initializeFacebook];
    
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
															 bundle: nil];
		
	MenuViewController *leftMenu = (MenuViewController*)[mainStoryboard
                                                         instantiateViewControllerWithIdentifier: @"MenuViewController"];
	leftMenu.view.backgroundColor = [UIColor whiteColor];
	leftMenu.cellIdentifier = @"leftMenuCell";
	
	[SlideNavigationController sharedInstance].leftMenu = leftMenu;
    [[SlideNavigationController sharedInstance] setEnableSwipeGesture:NO];
    // unity, return

    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:[PFFacebookUtils session]];
}

- (void)userLoginSignupViaSocialNetwork:(id)sender {
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Accesso..."
                                                   description:@"Tentativo di accesso in corso..."
                                                          type:TWMessageBarMessageTypeInfo callback:^{
                                                              NSLog(@"Message bar tapped!");
                                                          }];

    int social = [((UserLoginSignupViewController*)sender) social];
    if (social==0) {
        NSArray *permissions =  @[@"user_about_me", @"user_relationships", @"user_birthday", @"user_location"];
        [PFFacebookUtils logInWithPermissions:permissions block:^(PFUser *user, NSError *error) {
            if (!user) {
                NSLog(@"Uh oh. The user cancelled the Facebook login.");
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                               description:@"Errore durante l'accesso tramite Facebook."
                                                                      type:TWMessageBarMessageTypeError callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
                return;
            } else if (user.isNew) {
                NSLog(@"User signed up and logged in through Facebook!");
                FBRequest *request = [FBRequest requestForGraphPath:@"/me?fields=cover"];
                // Send request to Facebook
                [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    // handle response
                    NSDictionary *userData = (NSDictionary *)result;
                    NSString *cover_url = userData[@"cover"][@"source"];
                    [(Wuser*)user setTimeline_url:cover_url];
                    [user save];
                    [[self navigationController] popViewControllerAnimated:YES];
                }];
                
            } else {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Login eseguito!"
                                                               description:@"Accesso eseguito tramite Facebook."
                                                                      type:TWMessageBarMessageTypeSuccess callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
                [[self navigationController] popViewControllerAnimated:YES];
            }
        }];
    }
    else {
        [PFTwitterUtils logInWithBlock:^(PFUser *user, NSError *error) {
            if (!user) {
                NSLog(@"Uh oh. The user cancelled the Twitter login.");
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                               description:@"Errore durante l'accesso tramite Twitter."
                                                                      type:TWMessageBarMessageTypeError callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
                return;
            } else if (user.isNew) {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Login eseguito!"
                                                               description:@"Accesso eseguito tramite Twitter."
                                                                      type:TWMessageBarMessageTypeSuccess callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
                [[self navigationController] popViewControllerAnimated:YES];
            } else {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Login eseguito!"
                                                               description:@"Accesso eseguito tramite Twitter."
                                                                      type:TWMessageBarMessageTypeSuccess callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
                [[self navigationController] popViewControllerAnimated:YES];
            }    
        }];
    }
}

// Unity

- (void)playUnity
{
#warning UnityPause
    UnityAppController *appController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    [appController unityPause:NO];
}

- (void)pauseUnity
{
#warning UnityPause
    UnityAppController *appController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    [appController unityPause:YES];
}

// push notifications
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [super application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    
    // unity
    //UnityAppController *myUnityController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    //[myUnityController application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    [super application:application didFailToRegisterForRemoteNotificationsWithError:error];
    
    // unity
    //UnityAppController *myUnityController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    //[myUnityController application:application didFailToRegisterForRemoteNotificationsWithError:error];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [super application:application didReceiveRemoteNotification:userInfo];
    
    // unity
    //UnityAppController *myUnityController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    //[myUnityController application:application didReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication*)application didReceiveLocalNotification:(UILocalNotification*)notification
{
    [super application:application didReceiveLocalNotification:notification];
    
    // unity
    //UnityAppController *myUnityController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    //[myUnityController application:application didReceiveLocalNotification:notification];
}

-(void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    [self unityPause:YES];
    [super applicationDidReceiveMemoryWarning:application];
    //UnityAppController *myUnityController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    //[myUnityController applicationDidReceiveMemoryWarning:application];
}
-(void)applicationWillEnterForeground:(UIApplication *)application {
    [self unityPause:NO];
    [super applicationWillEnterForeground:application];
    //UnityAppController *myUnityController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    //[myUnityController applicationWillEnterForeground:application];
}
-(void)applicationWillResignActive:(UIApplication *)application {
    if (islaunched)
        [self unityPause:YES];
    [super applicationWillResignActive:application];
    //UnityAppController *myUnityController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    //[myUnityController applicationWillResignActive:application];
}
-(void)applicationWillTerminate:(UIApplication *)application {
    
}
- (void)applicationDidBecomeActive:(UIApplication *)application {
    [super applicationDidBecomeActive:application];
    //UnityAppController *myUnityController = (UnityAppController*) [[UIApplication sharedApplication] delegate];
    //[myUnityController applicationDidBecomeActive:application];
    [FBAppCall handleDidBecomeActiveWithSession:[PFFacebookUtils session]];
    if (islaunched)
        [self unityPause:NO];
    else
        islaunched = YES;
}
- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CoreDataModel" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"CoreDataModel.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (void)createViewHierarchyImpl
{
    UIStoryboard *storyBoard    = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *mainVC = [storyBoard instantiateInitialViewController];
    window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    window.rootViewController = mainVC;
    
    _rootController = [window rootViewController];
    _rootView   = _rootController.view;
    
    [self unityPause:YES];
}

@end
IMPL_APP_CONTROLLER_SUBCLASS(TNAppDelegate)
