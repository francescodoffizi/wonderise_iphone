//
//  WonderInsertCommentTableViewCell.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 19/05/14.
//
//

#import <UIKit/UIKit.h>
#import "Wonder.h"
@class WonderViewController;
@interface WonderInsertCommentTableViewCell : UITableViewCell <UITextViewDelegate> {
    IBOutlet UITextView *wonderComment;
    Wonder *wonder;
    WonderViewController *wvc;
}

@property (nonatomic, retain) UITextView *wonderComment;
@property (nonatomic, retain)     Wonder *wonder;
@property (nonatomic, retain)  WonderViewController *wvc;
@property (nonatomic, retain) UITableView *tableView;


-(IBAction)storeComment:(id)sender;


@end
