//
//  DetailFilterViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 12/05/14.
//
//

#import "DetailFilterViewController.h"
#import "AKSegmentedControl.h"
#import "MapFilterViewController.h"

@interface DetailFilterViewController ()

@end

@implementation DetailFilterViewController
@synthesize mfvc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
-(void)setupFilter:(int)type {
    currentFiltering = type;
    NSNumber *activeValue = [[[mfvc mvc] filterMask] objectForKey:[NSString stringWithFormat:@"f%d",currentFiltering]];
    for (UIView* subview in [self.view subviews]) {
        [subview removeFromSuperview];
    }
    switch (type) {
        case 0:
        {
            //Wonder Type Filter
            CGRect aRect = CGRectMake(0, self.view.frame.size.height/2-25 , 320, 50);
            AKSegmentedControl *segmentedControl = [[AKSegmentedControl alloc] initWithFrame:aRect];
            [segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
            
            // Setting the resizable background image
            /*
             UIImage *backgroundImage = [[UIImage imageNamed:@"segmented-bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)];
             [segmentedControl setBackgroundImage:backgroundImage];
             */
            // Setting the content edge insets to adapte to you design
            [segmentedControl setContentEdgeInsets:UIEdgeInsetsMake(2.0, 2.0, 3.0, 2.0)];
            
            // Setting the behavior mode of the control
            [segmentedControl setSegmentedControlMode:AKSegmentedControlModeSticky];
            
            // Setting the separator image
            //[segmentedControl setSeparatorImage:[UIImage imageNamed:@"segmented-separator.png"]];
            
            /*UIImage *buttonBackgroundImagePressedLeft = [[UIImage imageNamed:@"segmented-bg-pressed-left.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 4.0, 0.0, 1.0)];
             UIImage *buttonBackgroundImagePressedCenter = [[UIImage imageNamed:@"segmented-bg-pressed-center.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 4.0, 0.0, 1.0)];
             UIImage *buttonBackgroundImagePressedRight = [[UIImage imageNamed:@"segmented-bg-pressed-right.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 1.0, 0.0, 4.0)];
             */
            
            // Button 1
            UIButton *buttonInfo = [[UIButton alloc] init];
            UIImage *buttonInfoImageNormal = [UIImage imageNamed:@"filter_info_off.png"];
            UIImage *buttonInfoImageSelected = [UIImage imageNamed:@"filter_info_on.png"];
            
            [buttonInfo setImageEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, 0.0, 5.0)];
            
            [buttonInfo setImage:buttonInfoImageNormal forState:UIControlStateNormal];
            [buttonInfo setImage:buttonInfoImageSelected forState:UIControlStateSelected];
            [buttonInfo setImage:buttonInfoImageNormal forState:UIControlStateHighlighted];
            [buttonInfo setImage:buttonInfoImageNormal forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            // Button 2
            UIButton *buttonEco = [[UIButton alloc] init];
            UIImage *buttonEcoImageNormal = [UIImage imageNamed:@"filter_eco_off.png"];
            UIImage *buttonEcoImageSelected = [UIImage imageNamed:@"filter_eco_on.png"];
            
            [buttonEco setImage:buttonEcoImageNormal forState:UIControlStateNormal];
            [buttonEco setImage:buttonEcoImageSelected forState:UIControlStateSelected];
            [buttonEco setImage:buttonEcoImageNormal forState:UIControlStateHighlighted];
            [buttonEco setImage:buttonEcoImageNormal forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            // Button 3
            UIButton *buttonPlay = [[UIButton alloc] init];
            UIImage *buttonPlayImageNormal = [UIImage imageNamed:@"filter_play_off.png"];
            UIImage *buttonPlayImageSelected = [UIImage imageNamed:@"filter_play_on.png"];
            
            [buttonPlay setImage:buttonPlayImageNormal forState:UIControlStateNormal];
            [buttonPlay setImage:buttonPlayImageSelected forState:UIControlStateSelected];
            [buttonPlay setImage:buttonPlayImageNormal forState:UIControlStateHighlighted];
            [buttonPlay setImage:buttonPlayImageNormal forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            // Button 4
            UIButton *buttonTourism = [[UIButton alloc] init];
            UIImage *buttonTourismImageNormal = [UIImage imageNamed:@"filter_tourism_off.png"];
            UIImage *buttonTourismImageSelected = [UIImage imageNamed:@"filter_tourism_on.png"];
            
            [buttonTourism setImage:buttonTourismImageNormal forState:UIControlStateNormal];
            [buttonTourism setImage:buttonTourismImageSelected forState:UIControlStateSelected];
            [buttonTourism setImage:buttonTourismImageNormal forState:UIControlStateHighlighted];
            [buttonTourism setImage:buttonTourismImageNormal forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            // Button 5
            UIButton *buttonArtistic = [[UIButton alloc] init];
            UIImage *buttonArtisticImageNormal = [UIImage imageNamed:@"filter_artistic_off.png"];
            UIImage *buttonArtisticImageSelected = [UIImage imageNamed:@"filter_artistic_on.png"];
            
            [buttonArtistic setImage:buttonArtisticImageNormal forState:UIControlStateNormal];
            [buttonArtistic setImage:buttonArtisticImageSelected forState:UIControlStateSelected];
            [buttonArtistic setImage:buttonArtisticImageNormal forState:UIControlStateHighlighted];
            [buttonArtistic setImage:buttonArtisticImageNormal forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            
            // Setting the UIButtons used in the segmented control
            [segmentedControl setButtonsArray:@[buttonArtistic, buttonPlay, buttonEco, buttonTourism, buttonInfo]];
            if ([activeValue intValue]>-1)
                [segmentedControl setSelectedIndex:[activeValue intValue]];
            // Adding your control to the view
            [self.view addSubview:segmentedControl];
            break;
        }
        case 1:
            //Viewed wonder Filter
        {
            CGRect aRect = CGRectMake(0, self.view.frame.size.height/2-25 , 320, 50);
            AKSegmentedControl *segmentedControl = [[AKSegmentedControl alloc] initWithFrame:aRect];
            [segmentedControl addTarget:self action:@selector(segmentedControlValueChanged:) forControlEvents:UIControlEventValueChanged];
            
            // Setting the resizable background image
            /*
             UIImage *backgroundImage = [[UIImage imageNamed:@"segmented-bg.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0)];
             [segmentedControl setBackgroundImage:backgroundImage];
             */
            // Setting the content edge insets to adapte to you design
            [segmentedControl setContentEdgeInsets:UIEdgeInsetsMake(2.0, 2.0, 3.0, 2.0)];
            
            // Setting the behavior mode of the control
            [segmentedControl setSegmentedControlMode:AKSegmentedControlModeSticky];
            
            // Setting the separator image
            //[segmentedControl setSeparatorImage:[UIImage imageNamed:@"segmented-separator.png"]];
            
            /*UIImage *buttonBackgroundImagePressedLeft = [[UIImage imageNamed:@"segmented-bg-pressed-left.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 4.0, 0.0, 1.0)];
             UIImage *buttonBackgroundImagePressedCenter = [[UIImage imageNamed:@"segmented-bg-pressed-center.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 4.0, 0.0, 1.0)];
             UIImage *buttonBackgroundImagePressedRight = [[UIImage imageNamed:@"segmented-bg-pressed-right.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 1.0, 0.0, 4.0)];
             */
            
            // Button 1
            UIButton *buttonView = [[UIButton alloc] init];
            UIImage *buttonViewImageNormal = [UIImage imageNamed:@"filter_viewed_off.png"];
            UIImage *buttonViewImageSelected = [UIImage imageNamed:@"filter_viewed_on.png"];
            
            [buttonView setImage:buttonViewImageNormal forState:UIControlStateNormal];
            [buttonView setImage:buttonViewImageSelected forState:UIControlStateSelected];
            [buttonView setImage:buttonViewImageNormal forState:UIControlStateHighlighted];
            [buttonView setImage:buttonViewImageNormal forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            // Button 2
            UIButton *buttonViewed = [[UIButton alloc] init];
            UIImage *buttonViewedImageNormal = [UIImage imageNamed:@"filter_neverviewed_off.png"];
            UIImage *buttonViewedImageSelected = [UIImage imageNamed:@"filter_neverviewed_on.png"];
            
            [buttonViewed setImage:buttonViewedImageNormal forState:UIControlStateNormal];
            [buttonViewed setImage:buttonViewedImageSelected forState:UIControlStateSelected];
            [buttonViewed setImage:buttonViewedImageNormal forState:UIControlStateHighlighted];
            [buttonViewed setImage:buttonViewedImageNormal forState:(UIControlStateHighlighted|UIControlStateSelected)];
            
            // Setting the UIButtons used in the segmented control
            [segmentedControl setButtonsArray:@[buttonView, buttonViewed]];
            // Adding your control to the view
            if ([activeValue intValue]>-1)
                [segmentedControl setSelectedIndex:[activeValue intValue]];
            [self.view addSubview:segmentedControl];
            break;
        }
        case 2:
            //Disabled (Liked)
            
            break;
        case 3:
            //Disabled (Followed)
            
            break;
        case 4:
            //Distance Filter
            [self prepareSlider];
            break;
            
        default:
            break;
    }
}

-(void)prepareSlider{
    
#define WIDTH  282
#define HEIGHT 34
#define X_POS  20
#define Y_POS  242
#define RADIUS_POINT  10
#define SPACE_BETWEEN_POINTS  44.75
#define SLIDER_LINE_WIDTH     1
#define IPHONE_4_SUPPORT      88
    NSNumber *activeValue = [[[mfvc mvc] filterMask] objectForKey:[NSString stringWithFormat:@"f%d",currentFiltering]];
    if ([activeValue intValue]<0) {
        activeValue = [NSNumber numberWithInt:0];
    }
	CGRect sliderConrolFrame = CGRectNull;
    
	sliderConrolFrame = CGRectMake(X_POS,self.view.frame.size.height/2,WIDTH,HEIGHT);
    
    AKSSegmentedSliderControl* sliderControl = [[AKSSegmentedSliderControl alloc] initWithFrame:sliderConrolFrame];
    [sliderControl setDelegate:self];
	[sliderControl moveToIndex:[activeValue intValue]];
	[sliderControl setSpaceBetweenPoints:SPACE_BETWEEN_POINTS];
	[sliderControl setRadiusPoint:RADIUS_POINT];
    [sliderControl setStrokeSize:2.0];
    [sliderControl setStrokeColor:[UIColor clearColor]];
    
	[sliderControl setHeightLine:SLIDER_LINE_WIDTH];
    [self.view addSubview:sliderControl];
    
}

- (void)timeSlider:(AKSSegmentedSliderControl *)timeSlider didSelectPointAtIndex:(int)index{
    selectedValue = index;
    if (index==0) {
        index-=1;
    }
    [mfvc updateFilter:currentFiltering and:index];
}

- (void)segmentedControlValueChanged:(id)sender {
    AKSegmentedControl *aksc = sender;
    int selected = [[aksc selectedIndexes]firstIndex];
    [mfvc updateFilter:currentFiltering and:selected];
}

@end
