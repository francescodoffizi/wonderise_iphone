//
//  MetaForCreateTableViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 07/05/14.
//
//

#import <Parse/Parse.h>

@interface MetaForCreateTableViewController : PFQueryTableViewController {
    NSMutableArray *metas;
    IBOutlet UITableView *_tableView;
    NSDictionary *filters;
    NSMutableArray *favorites;
    PFQuery *metaquery;

}
-(void)loadMetas;

@property (nonatomic, retain) NSDictionary *filters;
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;

@end
