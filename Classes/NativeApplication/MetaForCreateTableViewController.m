//
//  MetaForCreateTableViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 07/05/14.
//
//

#import "MetaForCreateTableViewController.h"
#import "MetaForCreateCell.h"
#import "Meta.h"
#import "TNAppDelegate.h"
#import "TWMessageBarManager.h"
#import <CoreData/CoreData.h>
#import "FavouriteMeta.h"

@interface MetaForCreateTableViewController ()

@end

@implementation MetaForCreateTableViewController
@synthesize managedObjectContext,filters;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    filters = [[NSMutableDictionary alloc] init];
    [filters setValue:@"" forKey:@"name"];
    [filters setValue:@"" forKey:@"gallery"];
    [filters setValue:@"name" forKey:@"filterby"];
    [self loadMetas];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [metas count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
                        object:(PFObject *)object {
    [super tableView:tableView cellForRowAtIndexPath:indexPath object:object];
    static NSString *CellIdentifier = @"MetaForCreateCell";
    MetaForCreateCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    Meta *meta = (Meta*)object;
    NSString *dateString = [NSDateFormatter localizedStringFromDate:[meta createdAt]
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:nil];
    
    [cell setMetaDataWith:[[[meta user] fetchIfNeeded] valueForKey:@"name"] and:dateString and:[[meta name] uppercaseString] and:[(PFFile*)[meta thumbnail] url] and:meta.objectId];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [super tableView:tableView didSelectRowAtIndexPath:indexPath];
    if (indexPath.row<[self.objects count]) {
        TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        UnityIOSBridge *unityBridge = [appDelegate unityBridge];
        Meta *meta = [self.objects objectAtIndex:indexPath.row];
        [meta fetchIfNeeded];
        NSString *fileUrl = [NSString stringWithFormat:@"{\"url\": \"%@\",\"metaID\": \"%@\"}", [(PFFile*)[meta contentObjiOS] url], [meta objectId]];
#warning UnityPause
        [appDelegate unityPause:NO];
        [unityBridge setMetaToSave:meta];
        [unityBridge loadCreationScene:fileUrl];
        //[[self navigationController] pushViewController:[appDelegate unityVC] animated:YES];
        [[appDelegate unityVC] setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
        [self presentViewController:[appDelegate unityVC] animated:YES completion:^{
            NSLog(@"Ho Finito");
        }];
    }
}

- (PFQuery *)queryForTable {
    
    PFQuery *query;
    if (metaquery)
        query = metaquery;
    else
        query = [[Meta query] retain];
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.objects.count == 0) {
        query.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
//    [query orderByDescending:@"createdAt"];
    
    return query;
}

-(void)loadMetas {
    [self showLoader];
    metas = [[NSMutableArray alloc] init];
    metaquery = [[Meta query] retain];
//    metaquery.cachePolicy = kPFCachePolicyCacheThenNetwork;

    
    /*PFQuery *metaquery = [PFQuery queryWithClassName:@"Meta"];
     [metaquery whereKey:@"user"
     equalTo:[PFObject objectWithoutDataWithClassName:@"User" objectId:[user objectId]]];*/

    if (![[filters valueForKey:@"gallery"] isEqualToString:@""]) {
        TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
        NSManagedObjectContext *context = [appDelegate managedObjectContext];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"FavouriteMeta"
                                                  inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        favorites = [[NSMutableArray alloc] init];
        for (FavouriteMeta *favouriteMeta in fetchedObjects) {
            [favorites addObject:favouriteMeta.objectId];
        }
        PFQuery *favouritesQuery= [[Meta query] retain];
        [favouritesQuery whereKey:@"objectId" containedIn:favorites];
        
        PFQuery *mineQuery= [[Meta query] retain];
        [mineQuery whereKey:@"user" equalTo:[PFUser currentUser]];
        metaquery = [PFQuery orQueryWithSubqueries:@[favouritesQuery,mineQuery]];
//        metaquery.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    if (![[filters valueForKey:@"name"] isEqualToString:@""]) {
        if ([[filters valueForKey:@"filterby"] isEqualToString:@"user"]) {
            PFQuery *userQuery = [Wuser query];
            [userQuery whereKey:@"usernameNormalized" containsString:[filters valueForKey:@"name"]];
            [metaquery whereKey:@"user" matchesQuery:userQuery];
        }
        else {
            [metaquery whereKey:@"nameNormalized" containsString:[filters valueForKey:@"name"]];
        }
    }
    [metaquery orderByDescending:@"createdAt"];

    [metaquery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                for (Meta *meta in objects) {
                    /* fai qualcosa co sti meta */
                    [metas addObject:meta];
                }
                //[_tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
                [self loadObjects];
                [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
            });
        }
        else {
            metaquery = nil;
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                           description:@"Errore durante il recupero dei Meta."
                                                                  type:TWMessageBarMessageTypeError callback:^{
                                                                      NSLog(@"Message bar tapped!");
                                                                  }];
            
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

-(void)showLoader {
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Attendere!"
                                                   description:@"Caricamento dei Meta in corso..."
                                                          type:TWMessageBarMessageTypeInfo callback:^{
                                                              NSLog(@"Message bar tapped!");
                                                          }];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(id)initWithCoder:(NSCoder *)aDecoder {
    [super initWithCoder:aDecoder];
    if (self) {
        // This table displays items in the Todo class
        self.parseClassName = @"Meta";
        self.pullToRefreshEnabled = YES;
        self.paginationEnabled = YES;
        self.objectsPerPage = 25;
    }
    return self;
}

/*
- (PFQuery *)queryForTable {

    metas = [[NSMutableArray alloc] init];
    if (!metaquery) {
        metaquery = [Meta query];
    }
    // If no objects are loaded in memory, we look to the cache first to fill the table
    // and then subsequently do a query against the network.
    if (self.objects.count == 0) {
        metaquery.cachePolicy = kPFCachePolicyCacheThenNetwork;
    }
    
    return metaquery;
}
*/

- (void)objectsWillLoad {
    [super objectsWillLoad];
    // This method is called before a PFQuery is fired to get more objects
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Attendere!"
                                                   description:@"Caricamento dei meta in corso..."
                                                          type:TWMessageBarMessageTypeInfo callback:^{
                                                              NSLog(@"Message bar tapped!");
                                                          }];
}

- (void)objectsDidLoad:(NSError *)error {
    [super objectsDidLoad:error];
    // This method is called every time objects are loaded from Parse via the PFQuery
    if (error) {
        [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                       description:@"Errore durante il recupero dei Meta."
                                                              type:TWMessageBarMessageTypeError callback:^{
                                                                  NSLog(@"Message bar tapped!");
                                                              }];
    }
}


@end
