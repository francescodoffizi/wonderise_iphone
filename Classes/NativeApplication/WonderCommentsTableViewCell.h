//
//  WonderCommentsTableViewCell.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 19/05/14.
//
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
@interface WonderCommentsTableViewCell : UITableViewCell {
    IBOutlet AsyncImageView *profileImageView;
    IBOutlet UILabel *userName;
    IBOutlet UITextView *wonderComment;
    IBOutlet UIImageView *socialIcon;
    IBOutlet UIButton *profileViewBtn;
}
@property (nonatomic, retain) AsyncImageView *profileImageView;
@property (nonatomic, retain) UILabel *userName;
@property (nonatomic, retain) UITextView *wonderComment;
@property (nonatomic, retain) UIImageView *socialIcon;
@property (nonatomic, retain) UIButton *profileViewBtn;

@end
