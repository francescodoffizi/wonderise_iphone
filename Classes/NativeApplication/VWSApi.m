//
//  VWSApi.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 25/04/14.
//
//

#import "VWSApi.h"

#import "AFNetworking.h"

#import "Meta.h"

#define accessKey @"5b2c2c23d7a6fcd29399113fbcedc508279213a7"
#define secretKey @"4e430bd6380432b5134529d841ca364e9d637e0b"
#define apiUrl @"https://vws.vuforia.com"
//#define apiUrl @"http://10.0.1.43:8888/index.php?"

#include <CommonCrypto/CommonHMAC.h>
#import "base64.h"
#import "NSString+MD5.h"

#import "TWMessageBarManager.h"

#import "WonderViewController.h"


@implementation VWSApi

-(void) postTarget:(Wonder*)wonder {
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager manager] retain];
    AFJSONRequestSerializer *requestSerializer = [[AFJSONRequestSerializer serializer] retain];

    NSString *requestDate = [self getUTCFormateDate:[NSDate date]];

    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:requestDate forHTTPHeaderField:@"Date"];

    manager.requestSerializer = requestSerializer;
    
    NSMutableDictionary *wonderData = [[[NSMutableDictionary alloc] init] retain];
    
    PFFile *imageFile = [wonder image];
    
    UIImage *image = [UIImage imageWithData:[imageFile getData]];
    
    //image = [UIImage imageNamed:@"arrow.png"];
    
    NSData * data = [UIImagePNGRepresentation(image) base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    /*NSString *filePath = [[NSBundle mainBundle] pathForResource:@"arrow" ofType:@"png"];
    data = [[NSData dataWithContentsOfFile:filePath] base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    */
    data = [[NSData dataWithData:[imageFile getData]] base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    [wonderData setObject:[wonder objectId] forKey:@"name"];
    [wonderData setObject:[NSNumber numberWithFloat:image.size.width] forKey:@"width"];
    [wonderData setObject:[NSString stringWithUTF8String:[data bytes]] forKey:@"image"];
    [wonderData setObject:[NSNumber numberWithInt:1] forKey:@"active_flag"];
    
    
    NSError *error = [[[NSError alloc] init] retain];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:wonderData options:nil error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    NSString *requestUrl = [NSString stringWithFormat:@"%@/%@",apiUrl,@"targets"];
    [requestSerializer setValue:[NSString stringWithFormat:@"VWS %@:%@",accessKey,[self tmsSignature:jsonString andUrl:@"/targets" andDate:requestDate andSecret:secretKey andMethod:@"POST" andContentType:@"application/json"]] forHTTPHeaderField:@"Authorization"];
    
    [manager POST:requestUrl parameters:wonderData success:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSLog(@"Success: %@", responseObject);
        NSDictionary *resposeDict = (NSDictionary*)responseObject;
        [wonder setIsTargetReady:NO];
        [wonder setTargetID:[resposeDict valueForKey:@"target_id"]];
        [wonder saveEventually:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Conferma!"
                                                               description:@"Wonder creato con successo."
                                                                      type:TWMessageBarMessageTypeSuccess callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
            }
            else {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                               description:@"Problema durante la creazione del Wonder."
                                                                      type:TWMessageBarMessageTypeError callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
                [wonder deleteInBackground];
            }
        }];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//        NSLog([operation responseString]);
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                       description:@"Problema durante la creazione del Wonder."
                                                              type:TWMessageBarMessageTypeError callback:^{
                                                                  NSLog(@"Message bar tapped!");
                                                              }];
        [wonder deleteInBackground];
    }];
}

-(NSString*)tmsSignature:(NSString *)payLoad andUrl:(NSString*)urlString andDate:(NSString*)dateString andSecret:(NSString*)key andMethod:(NSString *)reqMethod andContentType:(NSString*)reqContentType {
    NSString *response;
    
    NSString *method = reqMethod;
    
    NSString *contentType = reqContentType;
    
    NSString *hexDigest = @"d41d8cd98f00b204e9800998ecf8427e";
    
    hexDigest = [[payLoad MD5] lowercaseString];

    NSString *requestPath = urlString;
    NSString *dateValue = dateString;
    NSString *toDigest = [NSString stringWithFormat:@"%@\n%@\n%@\n%@\n%@",method,hexDigest,contentType,dateValue,requestPath];
    
    response = [self hmacsha1:toDigest key:secretKey];

    return response;
}

- (NSString *)hmacsha1:(NSString *)text key:(NSString *)secret {
    NSData *secretData = [secret dataUsingEncoding:NSUTF8StringEncoding];
    NSData *clearTextData = [text dataUsingEncoding:NSUTF8StringEncoding];
    unsigned char result[20];
    CCHmac(kCCHmacAlgSHA1, [secretData bytes], [secretData length], [clearTextData bytes], [clearTextData length], result);
    
    char base64Result[32];
    size_t theResultLength = 32;

    Base64EncodeData(result, 20, base64Result, &theResultLength, NO);
    NSData *theData = [NSData dataWithBytes:base64Result length:theResultLength];
    NSString *base64EncodedResult = [[NSString alloc] initWithData:theData encoding:NSASCIIStringEncoding];
    
    return base64EncodedResult;
}

-(NSString *)getUTCFormateDate:(NSDate *)localDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en"]];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    [dateFormatter setDateFormat:@"ccc, dd MMM yyyy HH:mm:ss"];
    NSString *dateString = [dateFormatter stringFromDate:localDate];
    [dateFormatter release];

    return [NSString stringWithFormat:@"%@ GMT",dateString];
}

-(void) getTargetInfo:(Wonder*)wonder respondingTo:(id)caller {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    AFJSONRequestSerializer *requestSerializer = [[AFJSONRequestSerializer serializer] retain];
    
    NSString *requestDate = [self getUTCFormateDate:[NSDate date]];
    
    //[requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [requestSerializer setValue:requestDate forHTTPHeaderField:@"Date"];
    NSString *uriRequest = [NSString stringWithFormat:@"/targets/%@",[wonder targetID]];
    
    manager.requestSerializer = requestSerializer;
    [requestSerializer setValue:[NSString stringWithFormat:@"VWS %@:%@",accessKey,[self tmsSignature:@"" andUrl:uriRequest andDate:requestDate andSecret:secretKey  andMethod:@"GET" andContentType:@""]] forHTTPHeaderField:@"Authorization"];

    
    [manager GET:[NSString stringWithFormat:@"https://vws.vuforia.com/%@",uriRequest] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {        
        if ([[responseObject objectForKey:@"status"] isEqualToString:@"success"]) {
            [wonder setIsTargetReady:YES];
            [(WonderViewController*)caller updateReadyWonderStatus:YES];
            [wonder saveInBackground];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

@end
