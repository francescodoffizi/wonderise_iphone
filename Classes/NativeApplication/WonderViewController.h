//
//  WonderViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 23/04/14.
//
//

#import <UIKit/UIKit.h>
#import "WonderCommentsTableViewController.h"
#import "Wonder.h"
#import "AsyncImageView.h"
#import "CommentsContainerView.h"

@interface WonderViewController : UIViewController {
    IBOutlet CommentsContainerView *commentsContainer;
    NSMutableArray *commentsArray;
    IBOutlet UIScrollView *scrollView;
    IBOutlet WonderCommentsTableViewController *wctvc;
    Wonder *wonder;
    
    IBOutlet AsyncImageView *wonderImage;
    
    IBOutlet UILabel *notReadyWonder;
    IBOutlet UIButton *wonderPlay;
    IBOutlet UIButton *metaWonderBtn;
    IBOutlet UIButton *editWonderBtn;
    
    IBOutlet AsyncImageView *pictureProfileImgView;

    IBOutlet UITextView *wonderDescription;
    IBOutlet UILabel *wonderName;
    
    IBOutlet UILabel *commentsCount;
    IBOutlet UILabel *viewsCount;
    IBOutlet UILabel *likeCount;

    IBOutlet UILabel *wonderCategory;
    IBOutlet UILabel *wonderPostDate;

    
    UITextView *activeField;
}

-(IBAction)showWonder:(id)sender;
-(IBAction)checkWonderState:(id)sender;
-(IBAction)switchMetaWonder:(id)sender;
-(void)updateReadyWonderStatus:(BOOL)status;

-(IBAction)editWonder:(id)sender;

-(void)reloadWonderComments;

-(IBAction)likeWonder:(id)sender;

-(IBAction)showWonderOwner:(id)sender;

@property (nonatomic, retain) Wonder *wonder;
@end
