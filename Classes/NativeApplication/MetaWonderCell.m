//
//  MetaWonderCell.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 07/02/14.
//
//

#import "MetaWonderCell.h"

@implementation MetaWonderCell
@synthesize date, objectImageView,objectType,likesNumber,commentsNumber,viewsNumber, location, usefulObject,cellType,followersNumber, cellBackground;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setElementType:(NSString*)type {
    if (type) {
        [cellBackground setImage:[UIImage imageNamed:[NSString stringWithFormat:@"wonderinfo_background_%@",type]]];
    }
    else {
        [cellBackground setImage:nil];
    }
}

@end
