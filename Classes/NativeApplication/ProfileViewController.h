//
//  ProfileViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 07/02/14.
//
//

#import <UIKit/UIKit.h>
#import "MBProgressHUD.h"
#import "Wuser.h"

#import "AsyncImageView.h"
@interface ProfileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UILabel *username;
    IBOutlet UILabel *email;
    
    IBOutlet AsyncImageView *headerProfileImgView;
    IBOutlet AsyncImageView *pictureProfileImgView;
    
    IBOutlet UILabel *likeNumber;
    IBOutlet UILabel *commentsNumber;
    IBOutlet UILabel *visitsNumber;
    IBOutlet UILabel *followersNumber;
    
    NSMutableArray *metas;
    NSMutableArray *wonders;
    NSMutableArray *followers;
    
    IBOutlet UITableView *_tableView;
    
    int queryType;
    MBProgressHUD *hud;
    
    IBOutlet UIButton *commentsOrderBtn;
    IBOutlet UIButton *viewOrderBtn;
    IBOutlet UIButton *likeOrderBtn;

    IBOutlet UIButton *viewWonderBtn;
    IBOutlet UIButton *viewMetaBtn;
    IBOutlet UIButton *viewFollowerBtn;
    IBOutlet UIButton *userProfileEditBtn;

    IBOutlet UIButton *followBtn;

    
    Wuser *currentViewControllerUser;
    Wuser *userToShow;
    
}

@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *metas;
@property (nonatomic, retain) NSMutableArray *wonders;
@property (nonatomic, retain) NSMutableArray *followers;
@property (nonatomic, retain) Wuser *userToShow;

-(void)showLoader;

-(IBAction)orderWondersBy:(id)sender;
-(IBAction)viewMetas:(id)sender;
-(IBAction)viewFollowers:(id)sender;

-(void)toggleButtons:(int)btn;
-(void)updateUserdata:(Wuser*)user;

@end
