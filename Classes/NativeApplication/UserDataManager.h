//
//  UserDataManager.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 05/02/14.
//
//

#import <Foundation/Foundation.h>
#import <Parse/Parse.h>

@interface UserDataManager : NSObject

-(PFUser*)isUserLogged;

@end
