//
//  MetaForCreateViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 18/04/14.
//
//

#import "MetaForCreateViewController.h"
#import "MetaForCreateCell.h"
#import "Meta.h"

#import "TNAppDelegate.h"
#import "TWMessageBarManager.h"

@interface MetaForCreateViewController ()

@end

@implementation MetaForCreateViewController
@synthesize metas,mfctvc;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    mfvc = [sb instantiateViewControllerWithIdentifier:@"metaFilterView"];
    [[mfvc view] setFrame:CGRectMake(0, self.view.frame.size.height-65, mfvc.view.frame.size.width, 197)];
    [mfvc setMetaForCreate:self];
    [self.view addSubview:mfvc.view];
    [self.view bringSubviewToFront:mfvc.view];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */


-(IBAction)filterMenu:(id)sender {
    [mfvc slideFilter:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"metaEmbed"]) {
        mfctvc = [segue destinationViewController];
        /*Qui devo passare al controller i dati sui commenti e la paginazione*/
    }
}
-(void)filterMetas:(MetaFilterViewController*)mfvc {
    NSDictionary *filters = [mfctvc filters];
    [filters setValue:[[mfvc metaName] text]  forKey:@"name"];
    if ([[mfvc privateGallery] isOn]) {
        [filters setValue:@"private" forKey:@"gallery"];
    }
    else {
        [filters setValue:@"" forKey:@"gallery"];
    }
    if ([[mfvc userSearch] isOn]) {
        [filters setValue:@"user" forKey:@"filterby"];
    }
    else {
        [filters setValue:@"name" forKey:@"filterby"];
    }
    [mfctvc loadMetas];
}


@end