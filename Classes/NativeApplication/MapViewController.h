//
//  MapViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 29/01/14.
//
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

#import "DataReader.h"
#import "DataReaderDelegate.h"

#import "ClusterAnnotationView.h"


#import "CCHMapClusterAnnotation.h"
#import "CCHMapClusterController.h"
#import "CCHMapClusterControllerDelegate.h"
#import "CCHCenterOfMassMapClusterer.h"

#import "CCHMapClusterController.h"
#import "SlidingMetaInfoViewController.h"

@interface MapViewController : UIViewController <DataReaderDelegate, CCHMapClusterControllerDelegate, MKMapViewDelegate>
{
    IBOutlet MKMapView *mapView;
    NSArray *currentAnnotations;
    BOOL isLoadingData;

    IBOutlet UIView *containerView;
    
    NSMutableDictionary *filterMask;
    
    IBOutlet UIView *smivcContainer;
    SlidingMetaInfoViewController *smivc;
    
}
@property (strong, nonatomic) CCHMapClusterController *mapClusterController;
@property (strong, nonatomic) id<CCHMapClusterer> mapClusterer;
@property (strong,nonatomic)     NSArray *currentAnnotations;
@property (nonatomic, retain) NSMutableDictionary *filterMask;
@property (strong, nonatomic) MKMapView *mapView;

-(void)showHideFilters:(id)sender andVC:(UIViewController*)vc;
-(void)updateFilter:(int)index and:(int)value;
-(void)showSlidingMetaVC:(SlidingMetaInfoViewController*)smivcToShow and:(BOOL)visibility;
-(IBAction)hideSlidingVC:(id)sender;
@end
