//
//  UnityIOSBridge.h
//  Unity-iPhone
//
//  Created by Francesco D'Offizi on 07/02/14.
//
//

#import <Foundation/Foundation.h>
#import "Meta.h"

void messageFromUnity(char *message);
void setWonderCreateSessionData(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float scale, char *path);
void closeUnityActivity(BOOL param);


@interface UnityIOSBridge : NSObject {
    Meta *metaToSave;
}
@property (nonatomic, retain) Meta *metaToSave;

-(void)loadCreationScene:(NSString *)dataToSend;
-(void)loadVisualizationScene:(NSString *)dataToSend;

@end
