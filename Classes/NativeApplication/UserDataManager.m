//
//  UserDataManager.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 05/02/14.
//
//

#import "UserDataManager.h"

@implementation UserDataManager


-(PFUser*)isUserLogged {
    return [PFUser currentUser];
}


@end
