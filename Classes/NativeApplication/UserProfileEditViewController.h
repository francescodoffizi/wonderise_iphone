//
//  UserProfileEditViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 23/05/14.
//
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"
#import "VPImageCropperViewController.h"

@interface UserProfileEditViewController : UIViewController <VPImageCropperDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate> {
    IBOutlet AsyncImageView *profileImageView;
    IBOutlet AsyncImageView *timelineImageView;
    int imageWorked;
    NSMutableDictionary *savingStatus;
    IBOutlet UIView *editingView;
    IBOutlet UIImageView *editingArrow;
    
    IBOutlet NSLayoutConstraint *topConstraint;
}

-(IBAction)editProfileImage:(id)sender;
-(IBAction)showPreview:(id)sender;
@end
