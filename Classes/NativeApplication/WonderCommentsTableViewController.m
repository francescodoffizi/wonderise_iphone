//
//  WonderCommentsTableViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 23/04/14.
//
//

#import "WonderCommentsTableViewController.h"
#import "WonderCommentsTableViewCell.h"
#import "WonderComment.h"
#import "Wuser.h"
#import "WonderInsertCommentTableViewCell.h"
#import "ProfileViewController.h"
#import "SlideNavigationController.h"
#import "TWMessageBarManager.h"

@interface WonderCommentsTableViewController ()

@end

@implementation WonderCommentsTableViewController
@synthesize comments, tableView, wvc, wonder;

- (void)viewDidLoad
{
    [super viewDidLoad];

    [tableView setScrollEnabled:NO];
    comments = [[NSMutableArray alloc] init];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    //[[self view] setFrame:CGRectMake(0, 0, 320, ([comments count]+1)*75)];

    if (comments!=nil)
        return [comments count]+1;
    else
        return 1;
}

-(IBAction)showUserProfile:(id)sender {
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ProfileViewController *upvc  = [sb instantiateViewControllerWithIdentifier:@"profileView"];
    [[(WonderComment*)[comments objectAtIndex:[sender tag]] user] fetchIfNeededInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (!error) {
            [upvc setUserToShow:[(WonderComment*)[comments objectAtIndex:[sender tag]] user]];
            [[SlideNavigationController sharedInstance] pushViewController:upvc animated:YES];
        }
        else {
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore" description:error.localizedDescription type:TWMessageBarMessageTypeError];
        }
    }];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row<[comments count]) {
        
        WonderCommentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commentCell" forIndexPath:indexPath];
//        [[self view] setFrame:CGRectMake(0, 0, 320, indexPath.row*cell.frame.size.height)];
        
        // Configure the cell...
        [[(WonderComment*)[comments objectAtIndex:indexPath.row] user] fetchIfNeeded];
        
        Wuser *user = (Wuser*)[(WonderComment*)[comments objectAtIndex:indexPath.row] user];
        [[cell profileImageView] setImageURL:[NSURL URLWithString:[(Wuser*)[(WonderComment*)[comments objectAtIndex:indexPath.row] user] picture_url]]];
        [[cell userName] setText:[user name]];
        [[cell wonderComment] setText:[(WonderComment*)[comments objectAtIndex:indexPath.row] text]];
        if ([user socialProfile] && [user socialProfile]>0) {
            int socialprof = [[user socialProfile] intValue];
            switch (socialprof) {
                case 1:
                    [[cell socialIcon] setImage:[UIImage imageNamed:@"facebook_icon"]];
                    break;
                case 2:
                    [[cell socialIcon] setImage:[UIImage imageNamed:@"facebook_icon"]];
                    break;
                default:
                    [[cell socialIcon] setImage:NULL];
                    break;
            }
        }
        else {
            [[cell socialIcon] setImage:NULL];
        }
        [[cell profileViewBtn] setTag:indexPath.row];

        return cell;
    }
    else {
        WonderInsertCommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"wonderInsertCommentTableCell" forIndexPath:indexPath];
        [cell setWonder:wonder];
        [cell setWvc:wvc];
        /*cella di inserimento commento*/
        return cell;
    }
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(void)viewDidLayoutSubviews
{
    CGFloat height = MAX(self.view.bounds.size.height, self.tableView.contentSize.height);
    self.dynamicTVHeight.constant = height;
    [self.view layoutIfNeeded];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([comments count]>0 && [comments count]>indexPath.row) {
        NSString *text = [(WonderComment*)[comments objectAtIndex:indexPath.row] text];
        UITextView *simulatedTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 230, 200)];
        [simulatedTextView setFont:[UIFont fontWithName:@"System" size:12.0f]];
        [simulatedTextView setText:text];
        float height = [self measureHeightOfUITextView:simulatedTextView];
        return MAX(75,height+35);
    }
    return 75;
}

- (CGFloat)measureHeightOfUITextView:(UITextView *)textView
{
    if (floor(NSFoundationVersionNumber) > NSFoundationVersionNumber_iOS_6_1)
    {
        // This is the code for iOS 7. contentSize no longer returns the correct value, so
        // we have to calculate it.
        //
        // This is partly borrowed from HPGrowingTextView, but I've replaced the
        // magic fudge factors with the calculated values (having worked out where
        // they came from)
        
        CGRect frame = textView.bounds;
        
        // Take account of the padding added around the text.
        
        UIEdgeInsets textContainerInsets = textView.textContainerInset;
        UIEdgeInsets contentInsets = textView.contentInset;
        
        CGFloat leftRightPadding = textContainerInsets.left + textContainerInsets.right + textView.textContainer.lineFragmentPadding * 2 + contentInsets.left + contentInsets.right;
        CGFloat topBottomPadding = textContainerInsets.top + textContainerInsets.bottom + contentInsets.top + contentInsets.bottom;
        
        frame.size.width -= leftRightPadding;
        frame.size.height -= topBottomPadding;
        
        NSString *textToMeasure = textView.text;
        if ([textToMeasure hasSuffix:@"\n"])
        {
            textToMeasure = [NSString stringWithFormat:@"%@-", textView.text];
        }
        
        // NSString class method: boundingRectWithSize:options:attributes:context is
        // available only on ios7.0 sdk.
        
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        [paragraphStyle setLineBreakMode:NSLineBreakByWordWrapping];
        
        NSDictionary *attributes = @{ NSFontAttributeName: textView.font, NSParagraphStyleAttributeName : paragraphStyle };
        
        CGRect size = [textToMeasure boundingRectWithSize:CGSizeMake(CGRectGetWidth(frame), MAXFLOAT)
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:attributes
                                                  context:nil];
        
        CGFloat measuredHeight = ceilf(CGRectGetHeight(size) + topBottomPadding);
        return measuredHeight;
    }
    else
    {
        return textView.contentSize.height;
    }
}

@end
