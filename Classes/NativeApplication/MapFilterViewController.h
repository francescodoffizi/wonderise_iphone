//
//  MapFilterViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 12/05/14.
//
//

#import <UIKit/UIKit.h>
#import "MapViewController.h"
#import "DetailFilterViewController.h"

@interface MapFilterViewController : UIViewController {
    MapViewController *mvc;
    IBOutlet DetailFilterViewController *dfvc;
    IBOutlet UIView *detailFilterContainer;
    
    IBOutlet UIButton *typeBtn;
    IBOutlet UIButton *viewBtn;
    IBOutlet UIButton *activeButton;
}

@property (nonatomic,retain) MapViewController *mvc;

-(void)updateFilter:(int)index and:(int)value;
@end
