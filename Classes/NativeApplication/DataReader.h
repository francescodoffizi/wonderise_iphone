//
//  DataReader.h
//  Macoun 2013
//
//  Created by Hoefele, Claus(choefele) on 20.09.13.
//  Copyright (c) 2013 Hoefele, Claus(choefele). All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@protocol DataReaderDelegate;

@interface DataReader : NSObject {
    BOOL isShowingLoadingAlert;
}
@property (nonatomic, strong) NSOperationQueue *operationQueue;

@property (nonatomic, strong) id<DataReaderDelegate> delegate;

- (void)startReadingJSON;   // 
- (void)startReadingCSV;
- (void)startReadingWonders;
- (void)startReadingWondersInRegion:(MKMapView *)mapView withFiltermask:(NSMutableDictionary*)filterMask;
-(void)resetMessageTimer:(NSTimer*)theTimer;

@end
