//
//  MapViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 29/01/14.
//
//

#import "MapViewController.h"

#import "TWMessageBarManager.h"

#import "Wonder.h"

#import "WonderAnnotation.h"

#import "MapFilterViewController.h"

@interface MapViewController ()

@end

@implementation MapViewController@synthesize mapView;
@synthesize currentAnnotations,filterMask;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    smivc = nil;
    currentAnnotations = nil;
    // Set up map clustering
    self.mapClusterController = [[CCHMapClusterController alloc] initWithMapView:self.mapView];
    self.mapClusterController.delegate = self;
    // Read annotations
    
    // 5000+ items near Berlin in JSON format
    /*    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(41.816221, 12.5);
     MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, 45000, 45000);
     */
    // 80000+ items in the US
    //    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(39.833333, -98.583333);
    //    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, 7000000, 7000000);
    //    [dataReader startReadingCSV];
    
    
    //self.mapView.region = region;
    filterMask = [[NSMutableDictionary alloc] initWithCapacity:5];
    for (int t=0; t<5; t++) {
        [filterMask setObject:[NSNumber numberWithInt:-1] forKey:[NSString stringWithFormat:@"f%d",t]];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSString *)mapClusterController:(CCHMapClusterController *)mapClusterController titleForMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    NSUInteger numAnnotations = mapClusterAnnotation.annotations.count;
    NSString *unit = numAnnotations > 1 ? @"annotations" : @"annotation";
    return [NSString stringWithFormat:@"%tu %@", numAnnotations, unit];
}

- (NSString *)mapClusterController:(CCHMapClusterController *)mapClusterController subtitleForMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    NSUInteger numAnnotations = MIN(mapClusterAnnotation.annotations.count, 5);
    NSArray *annotations = [mapClusterAnnotation.annotations.allObjects subarrayWithRange:NSMakeRange(0, numAnnotations)];
    NSArray *titles = [annotations valueForKey:@"title"];
    return [titles componentsJoinedByString:@", "];
}



- (void)dataReader:(DataReader *)dataReader addAnnotations:(NSArray *)annotations
{
    //    [self.mapView addAnnotations:annotations];
    if (!currentAnnotations) {
        currentAnnotations = annotations;
    }
    [self.mapClusterController addAnnotations:annotations withCompletionHandler:^{
            isLoadingData = false;
            currentAnnotations = annotations;
    }];
    
/*    if ([annotations count]>0) {
        currentAnnotations = [self.mapClusterController.annotations allObjects];
        [self.mapClusterController removeAnnotations:currentAnnotations withCompletionHandler:^{
        }];
    }
*/
}

-(void)showMapUpdating {
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Aggiornamento..."
                                                   description:@"Caricamento dei Wonder nell'area visibile..."
                                                          type:TWMessageBarMessageTypeInfo callback:^{
                                                              NSLog(@"Message bar tapped!");
                                                          }];
}

- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
    [self showSlidingMetaVC:nil and:NO];
}

- (void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated {
    @synchronized(self) {

    if (!isLoadingData) {
        isLoadingData = true;
        [self.mapClusterController removeAnnotations:self.mapClusterController.annotations.allObjects withCompletionHandler:^{
            DataReader *dataReader = [[DataReader alloc] init];
            dataReader.delegate = self;
                [dataReader startReadingWondersInRegion:mapView withFiltermask:filterMask];
        }];
    }
    }
#warning try again later?
}

- (void)mapClusterController:(CCHMapClusterController *)mapClusterController willReuseMapClusterAnnotation:(CCHMapClusterAnnotation *)mapClusterAnnotation
{
    ClusterAnnotationView *clusterAnnotationView = (ClusterAnnotationView *)[self.mapClusterController.mapView viewForAnnotation:mapClusterAnnotation];
    clusterAnnotationView.count = mapClusterAnnotation.annotations.count;
    
    
    if ([mapClusterAnnotation isOneLocation]) {
        [clusterAnnotationView setMvc:self];

        NSSet *anns = mapClusterAnnotation.annotations;
        WonderAnnotation *wonderAnnotation = (WonderAnnotation*)[[anns allObjects] objectAtIndex:0];
        NSString * type = (NSString*)[[(Wonder*)[wonderAnnotation wonder] objectForKey:@"type"] objectForKey:@"name"];
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"wonder_%@.png",type]];
        [clusterAnnotationView setWonderAnnotations:[NSArray arrayWithObject:wonderAnnotation]];
        [clusterAnnotationView setupImagePinWithImage:image];
    }
    else {
        [clusterAnnotationView setWonderAnnotations:[[mapClusterController annotations] allObjects]];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    MKAnnotationView *annotationView;
    
    if ([annotation isKindOfClass:CCHMapClusterAnnotation.class]) {
        static NSString *identifier = @"clusterAnnotation";
        
        ClusterAnnotationView *clusterAnnotationView = (ClusterAnnotationView *)[theMapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (clusterAnnotationView) {
            clusterAnnotationView.annotation = annotation;
        } else {
            clusterAnnotationView = [[ClusterAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
            clusterAnnotationView.canShowCallout = NO;
        }
        if ([(CCHMapClusterAnnotation*)annotation isOneLocation]) {
            NSSet *anns = [(CCHMapClusterAnnotation*)[clusterAnnotationView annotation] annotations];
            WonderAnnotation *wonderAnnotation = (WonderAnnotation*)[[anns allObjects] objectAtIndex:0];
            /*NSURL* imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.wi-fi4free.it/site/images/ricerca/icons/%@",[annotation_hotspot icon]]];*/
            [clusterAnnotationView setWonderAnnotations:[NSArray arrayWithObject:wonderAnnotation]];
            [clusterAnnotationView setMvc:self];
            NSString * type = (NSString*)[[(Wonder*)[wonderAnnotation wonder] objectForKey:@"type"] objectForKey:@"name"];
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"wonder_%@.png",type]];
            [clusterAnnotationView setupImagePinWithImage:image];
            clusterAnnotationView.canShowCallout = NO;
        }
        else {
            NSSet *anns = [(CCHMapClusterAnnotation*)[clusterAnnotationView annotation] annotations];
            [clusterAnnotationView setWonderAnnotations:[anns allObjects]];
        }
        CCHMapClusterAnnotation *clusterAnnotation = (CCHMapClusterAnnotation *)annotation;
        clusterAnnotationView.count = clusterAnnotation.annotations.count;
        annotationView = clusterAnnotationView;
    }
    
    return annotationView;
}

- (void)viewDidLayoutSubviews
{
    //[scrollView setContentSize:CGSizeMake(320, mapView.frame.size.height+200)];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    //check segue identifier is segueaction is added
    MapFilterViewController *mfvc = [segue destinationViewController];
    [mfvc setMvc:self];
}

-(void)showHideFilters:(id)sender andVC:(UIViewController*)vc {
    int sizeView = 170;
    [UIView beginAnimations: @"animateFilterIn" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: 0.5f];
    if([sender tag]==0) {
        [sender setTag:1];
        //mapView.frame = CGRectOffset(mapView.frame, 0,-sizeView);
        containerView.frame = CGRectOffset(containerView.frame, 0,-sizeView);
    }
    else {
        [sender setTag:0];
        //mapView.frame = CGRectOffset(mapView.frame, 0,sizeView);
        containerView.frame = CGRectOffset(containerView.frame, 0,sizeView);
        @synchronized(self) {
        if (!isLoadingData) {
            isLoadingData = true;
            [self.mapClusterController removeAnnotations:self.mapClusterController.annotations.allObjects withCompletionHandler:^{
                DataReader *dataReader = [[DataReader alloc] init];
                dataReader.delegate = self;
                
                    [dataReader startReadingWondersInRegion:mapView withFiltermask:filterMask];
                
            }];
        }
        }
    }
    [UIView commitAnimations];

}
-(void)updateFilter:(int)index and:(int)value {
    [filterMask setObject:[NSNumber numberWithInt:value] forKey:[NSString stringWithFormat:@"f%d",index]];
}

-(void)showSlidingMetaVC:(SlidingMetaInfoViewController*)smivcToShow and:(BOOL)visibility {
    if (smivc) {
        [UIView beginAnimations: @"animateOutSmivc" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: 0.3];
        [[smivc view] setFrame:CGRectMake(0, -110, 320, 110)];
        [UIView commitAnimations];
        [[smivc view] removeFromSuperview];
        smivc = nil;
        [smivcContainer setUserInteractionEnabled:NO];
    }
    if (visibility) {
    smivc = smivcToShow;
    [[smivc view] setFrame:CGRectMake(0, -110, 320, 123)];
    [smivcContainer addSubview:smivc.view];
    [smivcContainer setUserInteractionEnabled:YES];
    [UIView beginAnimations: @"animateInSmivc" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: 0.3];
    [[smivc view] setFrame:CGRectMake(0, 0, 320, 123)];
    [UIView commitAnimations];
    }
}
-(IBAction)hideSlidingVC:(id)sender {
    [self showSlidingMetaVC:nil and:NO];
    //apply filters!
}
@end
