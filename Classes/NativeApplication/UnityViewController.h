//
//  UnityViewController.h
//  Unity-iPhone
//
//  Created by Francesco D'Offizi on 29/09/14.
//
//

#import <UIKit/UIKit.h>
#import "UI/UnityView.h"
#import "UI/UnityViewControllerBase.h"
#include "UnityAppController+ViewHandling.h"

@interface UnityViewController : UIViewController

@end
