//
//  UserRegistrationViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 03/02/14.
//
//

#import <UIKit/UIKit.h>

@interface UserLoginSignupViewController : UIViewController {
    IBOutlet UITextField *username;
    IBOutlet UITextField *email;
    IBOutlet UITextField *password;
    
    int social;
}

-(IBAction)doLoginSignupViaSocialNetwork:(id)sender;

@property int social;

@end
