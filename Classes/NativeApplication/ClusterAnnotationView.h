//
//  ClusterAnnotationView.h
//  CCHMapClusterController Example iOS
//
//  Created by Hoefele, Claus(choefele) on 09.01.14.
//  Copyright (c) 2014 Claus Höfele. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Wonder.h"
#import "SlidingMetaInfoViewController.h"

@class MapViewController;

@interface ClusterAnnotationView : MKAnnotationView {
    NSArray *wonderAnnotations;
    SlidingMetaInfoViewController *smivc;
    MapViewController *mvc;
}

@property (assign, nonatomic) NSUInteger count;
@property (nonatomic,strong) UIImageView* imageView;
@property (nonatomic,strong) UIImageView* defaultImageView;
@property (nonatomic,strong) UIButton *btn;
@property (nonatomic,strong) NSArray *wonderAnnotations;

@property (nonatomic,strong) MapViewController *mvc;

-(void)addActionGrabber:(int)type;
- (void)removeLabel;
- (void)removeImage;
- (void)setupImagePinWithUrl:(NSURL*)url;
- (void)setupImagePinWithImage:(UIImage*)image;
- (void)setupDefaultImageAndCounter;
-(void)showWonder:(Wonder*)wonder;
-(void)moreClusterDetails:(id)sender;

@end
