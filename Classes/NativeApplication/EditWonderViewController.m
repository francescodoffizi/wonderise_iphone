//
//  EditWonderViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 29/04/14.
//
//

#import "EditWonderViewController.h"
#import "SGActionView.h"
#import "TWMessageBarManager.h"
#import "VWSApi.h"
#import "TNAppDelegate.h"

@interface EditWonderViewController ()

@end

@implementation EditWonderViewController
@synthesize unityData, wonder;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [wonderImageView setImage:nil];
    // Do any additional setup after loading the view.
    isKeyboardShown = NO;
    haveCategoryBeenLoaded = NO;
    if (!wonder) {
        selectedWonderType = nil;
        selectedCategory = 0;
        [wonderImageView setImage:[UIImage imageWithData:[NSData dataWithContentsOfFile:(NSString*)[unityData valueForKey:@"path"]]]];
    } else {
        selectedWonderType = (WonderType*)[wonder type];
    }
    PFQuery *wonderTypeQuery = [WonderType query];
    [wonderTypeQuery orderByAscending:@"Name"];
    
    [wonderTypeQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        wonderTypes = objects;
        haveCategoryBeenLoaded = YES;
        if (wonder) {
            [titleField setText:[wonder note]];
            [descriptionField setText:[wonder objectForKey:@"description"]];
            [categoryTextField setText:[(WonderType*)[wonder type] name]];
            [wonderImageView setImageURL:[NSURL URLWithString:[[wonder image] url]]];
            BOOL found = false;
            for (int i=0; i<[wonderTypes count] && !found; i++) {
                WonderType *wonderType = [wonderTypes objectAtIndex:i];
                if ([[wonderType name] isEqualToString:[(WonderType*)[wonder type] name]]) {
                    selectedCategory = [NSNumber numberWithInt:i];
                    return;
                }
            }
        }
    }];
    
    [self registerForKeyboardNotifications];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Informazioni"
                                                   description:@"Inserisci le informazioni sul tuo Wonder."
                                                          type:TWMessageBarMessageTypeInfo duration:5.0f callback:^{
                                                              NSLog(@"Salvo il wonder");
                                                          }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

-(IBAction)showCategoryMenu:(id)sender{
    [okCategory setImage:[UIImage imageNamed:@"editing_on"]];

    [self.view endEditing:YES];
    if (haveCategoryBeenLoaded) {
        NSMutableArray *itemTitles = [[NSMutableArray alloc] init];
        NSMutableArray *itemImages = [[NSMutableArray alloc] init];
        
        for (WonderType* wonderType in wonderTypes) {
            [itemTitles addObject:[wonderType name]];
            [itemImages addObject:[NSString stringWithFormat:@"wonder_%@",[wonderType name]]];
        }
        
        [SGActionView showSheetWithTitle:@"Sheet View"
                              itemTitles:itemTitles
                           itemSubTitles:nil
                           selectedIndex:selectedCategory
                          selectedHandle:^(NSInteger selected){
                              [self selectCategory:selected];
                          }];
    }
    else {
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Attendere"
                                                       description:@"Elenco categorie non ancora caricato, attendere."
                                                              type:TWMessageBarMessageTypeError duration:5.0f callback:^{
                                                                  NSLog(@"Salvo il wonder");
                                                              }];
    }
}

-(void)selectCategory:(NSInteger)selected {

    selectedCategory = [NSNumber numberWithInt:selected];
    selectedWonderType = [wonderTypes objectAtIndex:selected];
    [categoryTextField setText:[selectedWonderType name]];
    [self checkAndUpdateFieldsAlert];

}

-(void)checkAndUpdateFieldsAlert {
    if([[titleField text] length]>0) {
        [okTitle setImage:[UIImage imageNamed:@"check"]];
    }
    else {
        [okTitle setImage:[UIImage imageNamed:@"editing_off"]];
    }
    
    if ([[descriptionField text] length]>0) {
        [okDescription setImage:[UIImage imageNamed:@"check"]];
    }
    else {
        [okDescription setImage:[UIImage imageNamed:@"editing_off"]];
    }
    
    if (selectedWonderType) {
        [okCategory setImage:[UIImage imageNamed:@"check"]];
    }
    else {
        [okCategory setImage:[UIImage imageNamed:@"editing_off"]];
    }
}

-(IBAction)checkDataAndsaveWonder:(id)sender {
    #warning CHECK DATA!!!!
    BOOL validData = true;
    
    if([[titleField text] length]<1) {
        validData = false;
    }
    
    if ([[descriptionField text] length]<1) {
        validData = false;
    }
    
    if (!selectedWonderType) {
        validData = false;
    }
    
    if (validData) {
        [self saveWonder];
    } else {
        [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore"
                                                       description:@"Controlla di aver compilato tutti i dati necessari per salvare il Wonder."
                                                              type:TWMessageBarMessageTypeError duration:3.0f callback:^{
                                                              }];
    }
}
-(void)saveWonder {
    if (!wonder) {
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Salvataggio in corso..."
                                                   description:@"Sto creando il Wonder, l'operazione può richiedere alcuni istanti e proseguirà in background."
                                                          type:TWMessageBarMessageTypeInfo duration:5.0f callback:^{
                                                              NSLog(@"Salvo il wonder");
                                                          }];
    
    Wonder *wonderToSave = [[Wonder alloc] init];
    
    PFFile *file = [PFFile fileWithName:@"marker.png" data:[NSData dataWithContentsOfFile:(NSString*)[unityData valueForKey:@"path"]]];
    
    [file saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            [wonderToSave setIsPublished:[publicSwitch state]];

            [wonderToSave setImage:file];
            TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];

            [wonderToSave setMeta:[[appDelegate unityBridge]metaToSave]];
            [wonderToSave setType:selectedWonderType];
            [wonderToSave setNumComments:[NSNumber numberWithInt:0]];
            [wonderToSave setNumLikes:[NSNumber numberWithInt:0]];
            [wonderToSave setNumVisits:[NSNumber numberWithInt:0]];
            
            [wonderToSave setUser:[PFUser currentUser]];
            [wonderToSave setMetaPosition:[NSArray arrayWithObjects:(NSNumber*)[unityData valueForKey:@"posX"], (NSNumber*)[unityData valueForKey:@"posY"], (NSNumber*)[unityData valueForKey:@"posZ"], nil]];
            [wonderToSave setMetaRotation:[NSArray arrayWithObjects:(NSNumber*)[unityData valueForKey:@"rotX"], (NSNumber*)[unityData valueForKey:@"rotY"], (NSNumber*)[unityData valueForKey:@"rotZ"], nil]];
            
            [wonderToSave setMetaScale:(NSNumber*)[unityData valueForKey:@"scale"]];
            
            [wonderToSave setNote:[titleField text]];
            [wonderToSave setDescription:[descriptionField text]];
            
            [PFGeoPoint geoPointForCurrentLocationInBackground:^(PFGeoPoint *geoPoint, NSError *error) {
                [wonderToSave setLocation:geoPoint];
                
                /*
                 CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
                 CLLocation * location = [[CLLocation alloc] initWithLatitude:geoPoint.latitude longitude:geoPoint.longitude];
                 
                 [geoCoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
                 if (!error) {
                 
                 CLPlacemark *placemark = [placemarks objectAtIndex:0];
                 [wonderToSave setCity:placemark.locality];
                 [wonderToSave setCountry:placemark.country];
                 }
                 */
                
                [wonderToSave saveEventually:^(BOOL succeeded, NSError *error) {
                    if (!error) {
                        VWSApi *vwsApi = [[VWSApi alloc] init];
                        TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                        [appDelegate setVwsApi:vwsApi];
                        [vwsApi performSelectorInBackground:@selector(postTarget:) withObject:wonderToSave];
                    }
                    else {
                        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                                       description:@"Problema durante il salvataggio del Wonder."
                                                                              type:TWMessageBarMessageTypeError callback:^{
                                                                                  NSLog(@"Message bar tapped!");
                                                                              }];
#warning here we've got a lost pffile! No way to delete this? @Giulio Prina Girotti
                    }
                }];
            }];
            
            /*}];*/
            
        }
        else {
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                           description:@"Problema durante il salvataggio del Wonder."
                                                                  type:TWMessageBarMessageTypeError callback:^{
                                                                      NSLog(@"Message bar tapped!");
                                                                  }];
            return;
        }
    }];
    }
    else {
        
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Salvataggio in corso..."
                                                       description:@"Sto aggiornando il Wonder, l'operazione può richiedere alcuni istanti e proseguirà in background."
                                                              type:TWMessageBarMessageTypeInfo duration:5.0f callback:^{
                                                                  NSLog(@"Salvo il wonder");
                                                              }];
        [wonder setType:selectedWonderType];
        [wonder setNote:[titleField text]];
        [wonder setDescription:[descriptionField text]];
        [wonder saveEventually:^(BOOL succeeded, NSError *error) {
            if (error) {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                               description:@"Problema durante il salvataggio del Wonder."
                                                                      type:TWMessageBarMessageTypeError callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
            }
            else {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Operazione completata!"
                                                               description:@"Wonder aggiornato correttamente."
                                                                      type:TWMessageBarMessageTypeSuccess callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
            }
        }];
    }
    [[SlideNavigationController sharedInstance] popViewControllerAnimated:YES];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextFieldUp:YES];
    activeField = textField;
    [okTitle setImage:[UIImage imageNamed:@"editing_on"]];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextFieldUp:NO];
    [self checkAndUpdateFieldsAlert];
    activeField = nil;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    //hide the keyboard
    [textField resignFirstResponder];
    //return NO or YES, it doesn't matter
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView*)textView {
    [self animateTextFieldUp:YES];
    activeField = textView;
    [okDescription setImage:[UIImage imageNamed:@"editing_on"]];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    [self.view endEditing:YES];
    [self checkAndUpdateFieldsAlert];
    [self animateTextFieldUp:NO];
    activeField = nil;

}


-(void)animateTextFieldUp:(BOOL)up
{
#warning disabled method
    if (false && up != isKeyboardShown) {
        
        const int movementDistance = -128; // tweak as needed
        const float movementDuration = 0.3f; // tweak as needed
        int movement = (up ? movementDistance : -movementDistance);
        
        [UIView beginAnimations: @"animateTextField" context: nil];
        [UIView setAnimationBeginsFromCurrentState: YES];
        [UIView setAnimationDuration: movementDuration];
        self.view.frame = CGRectOffset(self.view.frame, 0, movement);
        //[doneBtn setFrame:CGRectOffset(doneBtn.frame, 0, movement*0.75)];
        [UIView commitAnimations];
        isKeyboardShown = up;
        //[[self view] setNeedsUpdateConstraints];
    }
}

- (void)registerForKeyboardNotifications

{

    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWasShown:)
     
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
     
                                             selector:@selector(keyboardWillBeHidden:)
     
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
    
}



// Called when the UIKeyboardDidShowNotification is sent.

- (void)keyboardWasShown:(NSNotification*)aNotification

{
    
    NSDictionary* info = [aNotification userInfo];
    
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible
    
    // Your app might not need or want this behavior.
    
    CGRect aRect = self.view.frame;
    
    aRect.size.height -= kbSize.height;
    
    if (!CGRectContainsPoint(aRect, CGPointMake(activeField.frame.origin.x, activeField.frame.origin.y+activeField.frame.size.height)) ) {
        
        [scrollView scrollRectToVisible:activeField.frame animated:YES];
        
    }
    
}



// Called when the UIKeyboardWillHideNotification is sent

// Called when the UIKeyboardWillHideNotification is sent

- (void)keyboardWillBeHidden:(NSNotification*)aNotification

{
    
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    scrollView.contentInset = contentInsets;
    
    scrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)viewDidLayoutSubviews
{
    [scrollView setContentSize:CGSizeMake(320, controllerViewContainer.frame.size.height)];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    NSError* fileDeleteError = nil;
    [[NSFileManager defaultManager] removeItemAtPath:(NSString*)[unityData valueForKey:@"path"] error:&fileDeleteError];
    if (fileDeleteError) {
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore..."
                                                       description:@"C'è stato un errore nell'eliminazione di dati temporanei, speriamo non accada nuovamente..."
                                                              type:TWMessageBarMessageTypeInfo duration:3.0f callback:^{
                                                                  NSLog(@"Salvo il wonder");
                                                              }];
    }
}
@end
