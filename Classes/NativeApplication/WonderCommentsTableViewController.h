//
//  WonderCommentsTableViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 23/04/14.
//
//

#import <UIKit/UIKit.h>
#import "Wonder.h"

@class WonderViewController;

@interface WonderCommentsTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *comments;
    IBOutlet UITableView *tableView;
    Wonder *wonder;
    WonderViewController *wvc;
}

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *dynamicTVHeight;

@property (nonatomic, retain) NSMutableArray *comments;
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) Wonder *wonder;
@property (nonatomic, retain) WonderViewController *wvc;
@end
