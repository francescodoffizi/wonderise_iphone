//
//  WonderToViewViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 02/05/14.
//
//

#import "WonderToViewViewController.h"
#import "MetaWonderCell.h"
#import "Wonder.h"
#import "WonderType.h"
#import "WonderViewController.h"
#import "SlideNavigationController.h"
#import "AFNetworking.h"
#import "VWSApi.h"

@interface WonderToViewViewController ()

@end

@implementation WonderToViewViewController
@synthesize wonders;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [wonders count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    Wonder *currentWonder = (Wonder*)[wonders objectAtIndex:indexPath.row];
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    WonderViewController *wvc  = [sb instantiateViewControllerWithIdentifier:@"wonderViewController"];
    if (![currentWonder isTargetReady]) {
        VWSApi *vwsApi = [[VWSApi alloc] init];
        [vwsApi getTargetInfo:currentWonder respondingTo:wvc];
    }
    [wvc setWonder:currentWonder];
    [[SlideNavigationController sharedInstance] pushViewController:wvc animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    CellIdentifier = @"BigWonderCell";
    
    MetaWonderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [cell setElementType:nil];
    
    // Configure the cell...
    id metaOrWonder;
    [[cell objectImageView] setImage:nil];
    
    Wonder *wonder = [wonders objectAtIndex:indexPath.row];
    metaOrWonder = wonder;
    
    WonderType *wonderType = (WonderType*)[wonder type];
    [wonderType fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
        if (object)
            [cell setElementType:[[object objectForKey:@"name"] lowercaseString]];
    }];
    
    [[cell commentsNumber] setText:[[wonder numComments] stringValue]];
    [[cell objectImageView] setImageURL:[NSURL URLWithString:[[wonder image] url]]];
    [[cell objectType] setText:[[metaOrWonder objectForKey:@"note"] uppercaseString]];
    
    [[cell likesNumber] setText:[[metaOrWonder numLikes] stringValue]];
    [[cell viewsNumber] setText:[[metaOrWonder numVisits] stringValue]];
    
    NSString *dateString = [NSDateFormatter localizedStringFromDate:[metaOrWonder createdAt]
                                                          dateStyle:NSDateFormatterMediumStyle
                                                          timeStyle:nil];
    
    [[cell date] setText:dateString];
    
    return cell;
}


/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */


@end
