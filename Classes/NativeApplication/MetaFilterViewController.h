//
//  MetaFilterViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 19/04/14.
//
//

#import <UIKit/UIKit.h>
@class MetaForCreateViewController;

@interface MetaFilterViewController : UIViewController {
    BOOL isFilterVisible;
    BOOL isKeyboardVisible;
    IBOutlet UISwitch *publicGallery;
    IBOutlet UISwitch *privateGallery;
    IBOutlet UISwitch *titleSearch;
    IBOutlet UISwitch *userSearch;
    IBOutlet UITextField *metaName;
    MetaForCreateViewController *metaForCreate;
}

- (void)slideIn:(UIGestureRecognizer *)sender;
- (void)slideOut:(UIGestureRecognizer *)sender;

-(IBAction)slideFilter:(id)sender;

-(IBAction)switchGallery:(id)sender;
-(IBAction)switchFiltering:(id)sender;

@property (nonatomic, retain) UISwitch *publicGallery;
@property (nonatomic, retain) UISwitch *privateGallery;
@property (nonatomic, retain) UISwitch *titleSearch;
@property (nonatomic, retain) UISwitch *userSearch;
@property (nonatomic, retain) UITextField *metaName;
@property (nonatomic, retain) MetaForCreateViewController *metaForCreate;

@end
