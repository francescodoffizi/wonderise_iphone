//
//  MainViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 17/01/14.
//
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"
#import "LAWalkthroughViewController.h"

@interface MainViewController : UIViewController <SlideNavigationControllerDelegate> {
    IBOutlet UIScrollView *scrollView;
    IBOutlet UIButton *createBtn;
    IBOutlet UIButton *exploreBtn;
    IBOutlet UIButton *profileBtn;
    IBOutlet UILabel *createLabel;
    IBOutlet UILabel *profileLabel;
    
    IBOutlet UIButton *loginBtn;
    IBOutlet UIButton *logoutBtn;
    IBOutlet UIButton *signupBtn;
    
    IBOutlet UIView *logoutView;
    IBOutlet UIView *loginSignupView;
    
    IBOutlet UIImageView *logoutPostImg;
    IBOutlet UIButton *logoutArrow;
    
    LAWalkthroughViewController *walkthrough;
}

-(IBAction)pushUnity:(id)sender;
- (void)enableBtns:(BOOL)enabled;
-(void)updateUserStatusBtns;
- (IBAction)showLogoutButton:(id)sender;
- (void) hideLogoutButton;

- (IBAction)doLogout:(id)sender;

- (void)createWalkthrough;
-(void)hideTutorial;

@end
