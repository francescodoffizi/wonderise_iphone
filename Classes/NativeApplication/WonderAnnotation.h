//
//  WonderAnnotationView.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 28/03/14.
//
//

#import <MapKit/MapKit.h>
#import "Wonder.h"

@interface WonderAnnotation : MKPointAnnotation {
    Wonder *wonder;
}
@property (nonatomic, retain) Wonder *wonder;
@end
