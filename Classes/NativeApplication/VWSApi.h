//
//  VWSApi.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 25/04/14.
//
//

#import <Foundation/Foundation.h>
#import "Wonder.h"

@interface VWSApi : NSObject

-(void) postTarget:(Wonder*)wonder;
-(NSString*)tmsSignature:(NSString *)payLoad andUrl:(NSString*)urlString andDate:(NSString*)dateString andSecret:(NSString*)key andMethod:(NSString *)reqMethod andContentType:(NSString*)reqContentType;
- (NSString *)hmacsha1:(NSString *)text key:(NSString *)secret;
-(NSString *)getUTCFormateDate:(NSDate *)localDate;
-(void) getTargetInfo:(Wonder*)wonder respondingTo:(id)caller;


@end
