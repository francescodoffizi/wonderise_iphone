//
//  MetaForCreateCell.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 18/04/14.
//
//

#import <UIKit/UIKit.h>
#import "AsyncImageView.h"

@interface MetaForCreateCell : UITableViewCell {
    IBOutlet UILabel *user;
    IBOutlet UILabel *date;
    IBOutlet UILabel *name;
    IBOutlet AsyncImageView *metaImageView;
    IBOutlet UIButton *preferredBtn;
    NSString *metaId;
}
@property (nonatomic,strong) NSManagedObjectContext* managedObjectContext;
-(void)setMetaDataWith:(NSString*)username and:(NSString*)metadate and:(NSString*)metaname and:(NSString*)imageUrl and:(NSString*)objectId;
-(IBAction)addToFavourite:(id)sender;
@end
