//
//  WonderInsertCommentTableViewCell.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 19/05/14.
//
//

#import "WonderInsertCommentTableViewCell.h"
#import "TWMessageBarManager.h"
#import "WonderComment.h"
#import "WonderViewController.h"

@implementation WonderInsertCommentTableViewCell
@synthesize wonderComment, wonder, tableView,wvc;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(IBAction)storeComment:(id)sender {
    if ([[wonderComment text] length]>0) {
        WonderComment *wc = [[WonderComment alloc] init];
        [wc setUser:(Wuser*)[PFUser currentUser]];
        [wc setWonder:wonder];
        [wc setText:wonderComment.text];
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Attendere" description:@"Salvataggio del commento in corso..." type:TWMessageBarMessageTypeInfo];
        
        [wc saveEventually:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Conferma" description:@"Commento salvato con successo" type:TWMessageBarMessageTypeSuccess];
                wonderComment.text = @"";
                [wvc reloadWonderComments];
            }
            else {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore" description:@"Problema durante il salvataggio del commento" type:TWMessageBarMessageTypeError];
            }
        }];
    }
    else {
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Attenzione" description:@"Non puoi salvare un commento vuoto!" type:TWMessageBarMessageTypeInfo];
        
    }
}

- (BOOL) textView: (UITextView*) textView
shouldChangeTextInRange: (NSRange) range
  replacementText: (NSString*) text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    if ([textView.text length]>=1024) {
        [[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Attenzione" description:@"Raggiunto il limite di lunghezza per il commento..." type:TWMessageBarMessageTypeError];
        return NO;
    }
    return YES;
}


@end
