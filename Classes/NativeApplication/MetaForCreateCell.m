//
//  MetaForCreateCell.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 18/04/14.
//
//

#import "MetaForCreateCell.h"
#import <CoreData/CoreData.h>
#import "FavouriteMeta.h"
#import "TNAppDelegate.h"

@implementation MetaForCreateCell
@synthesize managedObjectContext;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

-(void)setMetaDataWith:(NSString*)username and:(NSString*)metadate and:(NSString*)metaname and:(NSString*)imageUrl and:(NSString*)objectId {
    [user setText:username];
    [name setText:metaname];
    [date setText:metadate];
    metaId = objectId;
    [metaImageView setImageURL:[NSURL URLWithString:imageUrl]];
    TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"FavouriteMeta"
                                              inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    BOOL found = false;
    for (FavouriteMeta *favouriteMeta in fetchedObjects) {
        if ([favouriteMeta.objectId isEqualToString:metaId]) {
            found = true;
            [context deleteObject:favouriteMeta];
            [preferredBtn setTag:1];
            [preferredBtn setBackgroundColor:[UIColor redColor]];
        }
    }
    
}

-(IBAction)addToFavourite:(id)sender {
    TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSError *error;
    if ([sender tag]==0) { //aggiungo il meta alla lista
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"FavouriteMeta"
                                                  inManagedObjectContext:context];
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        [fetchRequest setEntity:entity];
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        BOOL found = false;
        for (FavouriteMeta *favouriteMeta in fetchedObjects) {
            if ([favouriteMeta.objectId isEqualToString:metaId]) {
                found = true;
            }
        }
        if (!found) {
            FavouriteMeta *fm = [NSEntityDescription
                                 insertNewObjectForEntityForName:@"FavouriteMeta"
                                 inManagedObjectContext:context];
            [fm setObjectId:metaId];
            
            if (![context save:&error]) {
                NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
            } else {
                [sender setTag:1];
                [sender setBackgroundColor:[UIColor redColor]];
            }
        }
    }
    else { //rimuovo il meta dalla lista
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"FavouriteMeta"
                                                  inManagedObjectContext:context];
        [fetchRequest setEntity:entity];
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        BOOL found = false;
        for (FavouriteMeta *favouriteMeta in fetchedObjects) {
            if ([favouriteMeta.objectId isEqualToString:metaId]) {
                found = true;
                [context deleteObject:favouriteMeta];
                if (![context save:&error]) {
                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
                } else {
                    [sender setTag:0];
                    [sender setBackgroundColor:[UIColor whiteColor]];
                }
            }
        }
    }
}


@end
