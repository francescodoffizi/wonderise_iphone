//
//  WonderCommentsTableViewCell.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 19/05/14.
//
//

#import "WonderCommentsTableViewCell.h"

@implementation WonderCommentsTableViewCell
@synthesize wonderComment, profileImageView, userName, socialIcon, profileViewBtn;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
