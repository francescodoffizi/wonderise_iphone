//
//  WonderToViewViewController.h
//  Wonderise
//
//  Created by Francesco D'Offizi on 02/05/14.
//
//

#import <UIKit/UIKit.h>

@interface WonderToViewViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *wonders;
    IBOutlet UITableView *_tableView;
    int queryType;

}
@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) NSMutableArray *wonders;


@end
