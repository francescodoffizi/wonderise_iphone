//
//  CommentsContainerView.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 20/05/14.
//
//

#import "CommentsContainerView.h"

@implementation CommentsContainerView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (CGSize)intrinsicContentSize
{
    if ([[self subviews] count]>0) {
        CGSize size = [[[self subviews] objectAtIndex:0] frame].size;
        return size;
    }
    else {
        return CGSizeMake(320, 75);
    }
}
@end
