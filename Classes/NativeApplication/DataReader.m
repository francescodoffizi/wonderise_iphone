//
//  DataReader.m
//  Macoun 2013
//
//  Created by Hoefele, Claus(choefele) on 20.09.13.
//  Copyright (c) 2013 Hoefele, Claus(choefele). All rights reserved.
//

#import "DataReader.h"

#import "DataReaderDelegate.h"

#import <MapKit/MapKit.h>

#define BATCH_COUNT 500
#define DELAY_BETWEEN_BATCHES 0.3

#import "Wonder.h"

#import "TNAppDelegate.h"

#import "TWMessageBarManager.h"

#import "WonderAnnotation.h"

#import "TWMessageBarManager.h"

@implementation DataReader


- (id)init
{
    self = [super init];
    if (self) {
        _operationQueue = [[[NSOperationQueue alloc] init] retain];
        _operationQueue.maxConcurrentOperationCount = 1;
        isShowingLoadingAlert = false;
    }
    
    return self;
}


- (void)startReadingJSON
{
    // Parse on background thread
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSString *file = [NSBundle.mainBundle pathForResource:@"Berlin-Data" ofType:@"json"];
        NSInputStream *inputStream = [NSInputStream inputStreamWithFileAtPath:file];
        [inputStream open];
        NSArray *dataAsJson = [NSJSONSerialization JSONObjectWithStream:inputStream options:0 error:nil];
        
        NSMutableArray *annotations = [NSMutableArray arrayWithCapacity:BATCH_COUNT];
        for (NSDictionary *annotationAsJSON in dataAsJson) {
            // Convert JSON into annotation object
            MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
            NSString *latitudeAsString = [annotationAsJSON valueForKeyPath:@"location.coordinates.latitude"];
            NSString *longitudeAsString = [annotationAsJSON valueForKeyPath:@"location.coordinates.longitude"];
            annotation.coordinate = CLLocationCoordinate2DMake(latitudeAsString.doubleValue, longitudeAsString.doubleValue);
            annotation.title = [annotationAsJSON valueForKeyPath:@"person.lastName"];
            
            [annotations addObject:annotation];
            
            if (annotations.count == BATCH_COUNT) {
                // Dispatch batch of annotations
                [self dispatchAnnotations:annotations];
                [annotations removeAllObjects];
            }
        }
        
        // Dispatch remaining annotations if any!
        if ([annotations count]>0) {
            [self dispatchAnnotations:annotations];
        }
    });
}


- (void)startReadingWonders
{
    [self startReadingWondersInRegion:nil];
}

-(void)resetMessageTimer:(NSTimer*)theTimer {
    TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    appDelegate.isShowingLoadingAlert = NO;
}

- (void)startReadingWondersInRegion:(MKMapView *)mapView withFiltermask:(NSMutableDictionary*)filterMask {
    
    PFQuery *query = [Wonder query];
    
    TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    isShowingLoadingAlert = appDelegate.isShowingLoadingAlert;
    //    UIViewController *vc = [[appDelegate navigationController] visibleViewController];
    
    if (!isShowingLoadingAlert) {
        appDelegate.isShowingLoadingAlert = YES;
        [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(resetMessageTimer:) userInfo:nil repeats:NO];
        
        [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Aggiornamento..."
                                                       description:@"Caricamento dei Wonder nell'area visibile..."
                                                              type:TWMessageBarMessageTypeInfo callback:^{
                                                                  NSLog(@"Message bar tapped!");
                                                              }];
    }
    if (mapView) {
        MKCoordinateRegion region = [mapView region];
        CLLocationCoordinate2D center = region.center;
        MKCoordinateSpan span = region.span;
        
        CLLocationCoordinate2D northWestCorner, southEastCorner, northEastCorner, southWestCorner;
        
        northWestCorner.latitude  = center.latitude  - (span.latitudeDelta  / 2.0);
        northWestCorner.longitude = center.longitude - (span.longitudeDelta / 2.0);
        southEastCorner.latitude  = center.latitude  + (span.latitudeDelta  / 2.0);
        southEastCorner.longitude = center.longitude + (span.longitudeDelta / 2.0);
        
        northEastCorner.latitude = center.latitude + (span.latitudeDelta  / 2.0);
        northEastCorner.longitude = center.longitude + (span.longitudeDelta / 2.0);
        
        southWestCorner.latitude = center.latitude - (span.latitudeDelta / 2.0);
        southWestCorner.longitude = center.longitude - (span.longitudeDelta / 2.0);
        
        if (southWestCorner.latitude<-90)
            southWestCorner.latitude = -89.999;
        if (southWestCorner.latitude>=90)
            southWestCorner.latitude = 89.999;
        
        if (northEastCorner.latitude<-90)
            northEastCorner.latitude = -89.999;
        if (northEastCorner.latitude>=90)
            northEastCorner.latitude = 89.999;
        
        PFGeoPoint *swOfSF = [PFGeoPoint geoPointWithLatitude:southWestCorner.latitude longitude:southWestCorner.longitude];
        PFGeoPoint *neOfSF = [PFGeoPoint geoPointWithLatitude:northEastCorner.latitude longitude:northEastCorner.longitude];
        
        [query includeKey:@"type"];
        //            [segmentedControl setButtonsArray:@[buttonArtistic, buttonPlay, buttonEco, buttonTourism, buttonInfo]];

        if ([(NSNumber*)[filterMask objectForKey:[NSString stringWithFormat:@"f%d",0]] intValue]>-1) { //wondertype filter
            NSString *wonderType;
            switch ([(NSNumber*)[filterMask objectForKey:[NSString stringWithFormat:@"f%d",0]] intValue]) {
                case 0:
                    wonderType = @"art";
                    break;
                case 1:
                    wonderType = @"play";
                    break;
                case 2:
                    wonderType = @"eco";
                    break;
                case 3:
                    wonderType = @"tourism";
                    break;
                case 4:
                    wonderType = @"info";
                    break;
                default:
                    break;
            }
            PFQuery *typequery = [WonderType query];
            [typequery whereKey:@"name" equalTo:wonderType];
            [query whereKey:@"type" matchesQuery:typequery];
            //[query whereKey:@"type" equalTo:[types objectAtIndex:0]];
        }
        
        if ([(NSNumber*)[filterMask objectForKey:[NSString stringWithFormat:@"f%d",1]] intValue]>-1) { //viewed-notviewed only
            PFQuery *wonderUserRelation = [WonderUserRelation query];
            [wonderUserRelation whereKey:@"user" equalTo:(Wuser*)[PFUser currentUser]];
            [wonderUserRelation whereKey:@"numVisits" greaterThan:[NSNumber numberWithInt:0]];
            switch ([(NSNumber*)[filterMask objectForKey:[NSString stringWithFormat:@"f%d",1]] intValue]) {
                case 1: //this seems to be not working at all
                {
                    [query whereKey:@"wonderID" doesNotMatchKey:@"wonder" inQuery:wonderUserRelation];
                }
                    break;
                case 0: //this seems to be working (Viewed)
                {
                    [query whereKey:@"wonderID" matchesKey:@"wonder" inQuery:wonderUserRelation];
                }
                    break;
                default:
                    break;
            }
        }
        // not working doesnotmatch https://parse.com/questions/trouble-with-nested-query-using-objectid
        
        if ([(NSNumber*)[filterMask objectForKey:[NSString stringWithFormat:@"f%d",2]] intValue]>-1) { //like only
            PFQuery *wonderUserRelation = [WonderUserRelation query];
            [wonderUserRelation whereKey:@"user" equalTo:(Wuser*)[PFUser currentUser]];
            [wonderUserRelation whereKey:@"likesTheWonder" equalTo:YES];
            [query whereKey:@"objectId" matchesKey:@"wonder" inQuery:wonderUserRelation];
        }
        
        if ([(NSNumber*)[filterMask objectForKey:[NSString stringWithFormat:@"f%d",3]] intValue]>-1) { //followers only
            [query whereKey:@"user" containedIn:[[[(Wuser*)[PFUser currentUser] usersFollowed] query] findObjects]]; //Warning mainthread!!!
        }
        
        if ([(NSNumber*)[filterMask objectForKey:[NSString stringWithFormat:@"f%d",4]] intValue]>-1) { //distance from here
            //1-5-10-20 meters
            int distance;
            switch ([(NSNumber*)[filterMask objectForKey:[NSString stringWithFormat:@"f%d",4]] intValue]) {
                case 1:
                    distance = 1;
                    break;
                case 2:
                    distance = 5;
                    break;
                case 3:
                    distance = 10;
                    break;
                case 4:
                    distance = 20;
                    break;
                default:
                    distance = 0;
                    break;
            }
            if (distance>0) {
                PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:[mapView centerCoordinate].latitude longitude:[mapView centerCoordinate].longitude];
                [query whereKey:@"location" nearGeoPoint:geoPoint withinKilometers:distance];
            }
            
        } else {
            [query whereKey:@"location" withinGeoBoxFromSouthwest:swOfSF toNortheast:neOfSF];
        }
        
        
        
        
        [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
            if (!error) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                    BOOL hasDispatched = false;
                    
                    NSMutableArray *annotations = [NSMutableArray arrayWithCapacity:objects.count];
                    for (Wonder *wonder in objects) {
                        
                        WonderAnnotation *annotation = [[WonderAnnotation alloc] init];
                        annotation.coordinate = CLLocationCoordinate2DMake([[wonder location] latitude],[[wonder location] longitude]);
                        annotation.title = [wonder note];
                        annotation.wonder = [wonder retain];
                        [annotations addObject:annotation];
                        
                        if (annotations.count == objects.count) {
                            // Dispatch batch of annotations
                            hasDispatched = true;
                            [self dispatchAnnotations:annotations];
                            [annotations removeAllObjects];
                        }
                    }
                    
                    // Dispatch remaining annotations if any!
                    if ([annotations count]>0 || !hasDispatched) {
                        [self dispatchAnnotations:annotations];
                    }
                });
            }
            else {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                               description:@"Errore durante il recupero dei Wonder."
                                                                      type:TWMessageBarMessageTypeError callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
                [self dispatchAnnotations:[NSMutableArray arrayWithCapacity:objects.count]];
                
                //            NSLog([error localizedDescription]);
            }
            //[[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
            
        }];
    }
    else  {
        //[[TWMessageBarManager sharedInstance] hideAllAnimated:YES];
    }
}


- (void)dispatchAnnotations:(NSArray *)annotations
{
    // Dispatch on main thread with some delay to simulate network requests
    NSArray *annotationsToDispatch = [annotations copy];
    [self.operationQueue addOperationWithBlock:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.delegate dataReader:self addAnnotations:annotationsToDispatch];
        });
        [NSThread sleepForTimeInterval:DELAY_BETWEEN_BATCHES];
    }];
}

@end
