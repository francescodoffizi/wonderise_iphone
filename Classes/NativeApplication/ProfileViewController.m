//
//  ProfileViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 07/02/14.
//
//

#import "ProfileViewController.h"
#import "TNAppDelegate.h"

#import "UIImage+RoundedImage.h"

#import "MetaWonderCell.h"

#import "TWMessageBarManager.h"
#import "WonderViewController.h"
#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController
@synthesize tableView = _tableView, metas, wonders, followers, userToShow;

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        currentViewControllerUser = nil;
        userToShow = nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    /*
     Circular mask to profile image
     */
    
    pictureProfileImgView.layer.borderWidth=3.0;
    pictureProfileImgView.layer.borderColor = [UIColor whiteColor].CGColor;
    pictureProfileImgView.layer.backgroundColor=[UIColor clearColor].CGColor;
    pictureProfileImgView.layer.cornerRadius=pictureProfileImgView.bounds.size.width/2;
    [pictureProfileImgView.layer setMasksToBounds:YES];

    [followBtn setAlpha:0.0f];
    [followBtn setUserInteractionEnabled:NO];

    queryType = 1;
    metas = [[NSMutableArray alloc] init];
    wonders = [[NSMutableArray alloc] init];
    followers = [[NSMutableArray alloc] init];
    
    [self toggleButtons:0];
    
    PFQuery *query= [[Wuser query] retain];
    
    /* [[PFUser currentUser]username] @"sf3diso319ld7fzs3uo82e5bs" */
    if (userToShow==nil) {
        currentViewControllerUser = (Wuser*)[PFUser currentUser];
        [userProfileEditBtn setAlpha:1.0f];
        [userProfileEditBtn setUserInteractionEnabled:YES];
        [self updateUserdata:currentViewControllerUser];
    }
    else {
        [query whereKey:@"username" equalTo:[userToShow username]];
        //Check if currentuser=viewedser and updating UI
        if ([[userToShow username] isEqualToString:[[PFUser currentUser]username]]) {
            [userProfileEditBtn setAlpha:1.0f];
            [userProfileEditBtn setUserInteractionEnabled:YES];
            [self updateUserdata:userToShow];
        }
        else {
            [query getFirstObjectInBackgroundWithBlock:^(PFObject *object, NSError *error){
                Wuser *user = (Wuser*) object;
                [self updateUserdata:user];
                PFRelation *relation = [(Wuser *)[PFUser currentUser] usersFollowed];
                PFQuery *relationQuery = [relation query];
                [relationQuery whereKey:@"objectId" equalTo:[currentViewControllerUser objectId]];
                [relationQuery countObjectsInBackgroundWithBlock:^(int number, NSError *error) {
                    [followBtn setAlpha:1.0f];
                    [followBtn setUserInteractionEnabled:YES];
                    
                    if (!error && number>0) {
                        [followBtn setTag:1];
                        [followBtn setTitle:@"- Followed" forState:UIControlStateNormal];
                    }
                }];
            }];
            [userProfileEditBtn setAlpha:0.0f];
            [userProfileEditBtn setUserInteractionEnabled:YES];
        }
        
    }
    
    

    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)updateUserdata:(Wuser*)user {
    
    currentViewControllerUser = [user retain];
    [email setText:[user email]];
    [username setText:[user name]];
    
    [likeNumber setText:[[user numLikes] stringValue]];
    [commentsNumber setText:[[user numComments] stringValue]];
    [visitsNumber setText:[[user numVisits] stringValue]];
    [followersNumber setText:[[user numFollowers] stringValue]];
    
    /* Se c'è il file dovremmo fare qualcos altro ma non so ancora cosa */
    PFFile *headerFile = [user timeline_file];
    if (headerFile) {
        [headerProfileImgView setImageURL:[NSURL URLWithString:[headerFile url]]];
    }
    else {
        [headerProfileImgView setImageURL:[NSURL URLWithString:[user timeline_url]]];
    }
    PFFile *pictureFile = [user picture_file];
    if (pictureFile) {
        [pictureProfileImgView setImageURL:[NSURL URLWithString:[pictureFile url]]];
    }
    else {
        [pictureProfileImgView setImageURL:[NSURL URLWithString:[user picture_url]]];
    }
    
    
    //PFRelation *relation = [user relationforKey:@"posts"];
    [self viewMetas:self];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    int count = 0;
    if (queryType==1) {
        count = [metas count];
    }
    else if (queryType==0) {
        count = [wonders count];
    }
    else if (queryType == 2) {
        count = [followers count];
    }
    return count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"Main"
                                                  bundle:nil];
    MetaWonderCell *cella = (MetaWonderCell*)[tableView cellForRowAtIndexPath:indexPath] ;
    switch ([cella cellType]) {
        case 0:
        {
            
            WonderViewController *wvc  = [sb instantiateViewControllerWithIdentifier:@"wonderViewController"];
            [wvc setWonder:(Wonder*)[wonders objectAtIndex:indexPath.row]];
            [[SlideNavigationController sharedInstance] pushViewController:wvc animated:YES];
        }
            break;
        case 2:
        {
            ProfileViewController *upvc  = [sb instantiateViewControllerWithIdentifier:@"profileView"];
            [upvc setUserToShow:(Wuser *)[cella usefulObject]];
            [[SlideNavigationController sharedInstance] pushViewController:upvc animated:YES];
        }
            break;
        default:
            break;
    }
    
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    switch (queryType) {
        case 0:
            CellIdentifier = @"WonderCell";
            break;
        case 1:
            CellIdentifier = @"MetaCell";
            break;
        default:
            CellIdentifier = @"FollowerCell";
            break;
    }
    MetaWonderCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [cell setElementType:nil];
    
    // Configure the cell...
    id metaOrWonder;
    [[cell objectImageView] setImage:nil];
    [cell setCellType:queryType];
    
    if (queryType==1) {
        Meta *meta = [metas objectAtIndex:indexPath.row];
        metaOrWonder = meta;
        [cell setUsefulObject:meta];
        [[cell objectImageView] setImageURL:[NSURL URLWithString:[[metaOrWonder thumbnail] url]]];
        [[cell objectType] setText:[[metaOrWonder name] uppercaseString]];
    }
    else if (queryType==0) {
        Wonder *wonder = [wonders objectAtIndex:indexPath.row];
        metaOrWonder = wonder;
        
        WonderType *wonderType = (WonderType*)[wonder type];
        [wonderType fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
            if (object)
                [cell setElementType:[[object objectForKey:@"name"] lowercaseString]];
        }];
        
        [cell setUsefulObject:wonder];
        [[cell commentsNumber] setText:[[wonder numComments] stringValue]];
        [[cell objectImageView] setImageURL:[NSURL URLWithString:[[wonder image] url]]];
        [[cell objectType] setText:[[metaOrWonder objectForKey:@"note"] uppercaseString]];
    }
    else {
        [cell setCellType:2];
        Wuser *user = [followers objectAtIndex:indexPath.row];
        [cell setUsefulObject:user];
        [[cell objectImageView] setImageURL:[NSURL URLWithString:[user picture_url]]];
        [cell.objectImageView setBackgroundColor:[UIColor whiteColor]];
        cell.objectImageView.layer.borderWidth=3.0;
        cell.objectImageView.layer.borderColor = [UIColor blackColor].CGColor;
        cell.objectImageView.layer.backgroundColor=[UIColor clearColor].CGColor;
        cell.objectImageView.layer.cornerRadius=10;
        [cell.objectImageView.layer setMasksToBounds:YES];
        [[cell cellBackground] setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.5]];
        
        [[cell followersNumber] setText:[[user numFollowers]stringValue]];
        [[cell objectType] setText:[[user name] uppercaseString]];
    }
    if ([cell cellType]!=2) {
        [[cell likesNumber] setText:[[metaOrWonder numLikes] stringValue]];
        [[cell viewsNumber] setText:[[metaOrWonder numVisits] stringValue]];
        NSString *dateString = [NSDateFormatter localizedStringFromDate:[metaOrWonder createdAt]
                                                              dateStyle:NSDateFormatterMediumStyle
                                                              timeStyle:nil];
        
        [[cell date] setText:dateString];
        
    }
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a story board-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 
 */


-(void)showLoader {
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading";
}

-(IBAction)viewFollowers:(id)sender {
    [self performSelectorOnMainThread:@selector(showLoader) withObject:nil waitUntilDone:NO];
    queryType = 2;
    [self toggleButtons:queryType];
    [followers release];
    followers = [[NSMutableArray alloc] init];
    
    PFQuery *userQuery= [[Wuser query] retain];
    
    [userQuery whereKey:@"user" equalTo:currentViewControllerUser];
    PFRelation *relation = [currentViewControllerUser relationforKey:@"usersFollowed"];
    // generate a query based on that relation
    userQuery = [relation query];
    /*PFQuery *metaquery = [PFQuery queryWithClassName:@"Meta"];
     [metaquery whereKey:@"user"
     equalTo:[PFObject objectWithoutDataWithClassName:@"User" objectId:[user objectId]]];*/
    
    [userQuery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [hud hide:YES];
        
        if (!error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                for (Wuser *user in objects) {
                    /* fai qualcosa co sti meta */
                    [followers addObject:[user retain]];
                }
                
                [[self tableView] performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
            });
        }
        else {
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                           description:@"Errore durante il recupero dei Meta dell'utente."
                                                                  type:TWMessageBarMessageTypeError callback:^{
                                                                      NSLog(@"Message bar tapped!");
                                                                  }];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
    
    
}

-(IBAction)viewMetas:(id)sender {
    [self performSelectorOnMainThread:@selector(showLoader) withObject:nil waitUntilDone:NO];
    queryType = 1;
    [self toggleButtons:queryType];
    
    metas = [[NSMutableArray alloc] init];
    PFQuery *metaquery= [[Meta query] retain];
    [metaquery whereKey:@"user" equalTo:currentViewControllerUser];
    
    /*PFQuery *metaquery = [PFQuery queryWithClassName:@"Meta"];
     [metaquery whereKey:@"user"
     equalTo:[PFObject objectWithoutDataWithClassName:@"User" objectId:[user objectId]]];*/
    
    [metaquery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [hud hide:YES];
        
        if (!error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                for (Meta *meta in objects) {
                    /* fai qualcosa co sti meta */
                    [metas addObject:[meta retain]];
                }
                
                [[self tableView] performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
            });
        }
        else {
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                           description:@"Errore durante il recupero dei Meta dell'utente."
                                                                  type:TWMessageBarMessageTypeError callback:^{
                                                                      NSLog(@"Message bar tapped!");
                                                                  }];
            [self dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

-(IBAction)orderWondersBy:(id)sender {
    [self performSelectorOnMainThread:@selector(showLoader) withObject:nil waitUntilDone:NO];
    queryType = 0;
    [self toggleButtons:queryType];
    [wonders release];
    wonders = [[NSMutableArray alloc] init];
    
    PFQuery *wonderquery= [Wonder query];
    [wonderquery whereKey:@"user" equalTo:currentViewControllerUser];
    
    switch ([sender tag]) {
        case 1: //wonderbylike
            [wonderquery orderByAscending:@"numLikes"];
            break;
        case 2: //wonderbycomments
            [wonderquery orderByAscending:@"numComments"];
            break;
        case 3: //wonderbyvisits
            [wonderquery orderByAscending:@"numVisits"];
            break;
        default:
            [wonderquery orderByDescending:@"createdAt"];
            break;
    }
    
    [wonderquery findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        [hud hide:YES];
        if (!error) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
                
                for (Wonder *wonder in objects) {
                    /* fai qualcosa co sti meta */
                    [wonders addObject:[wonder retain]];
                }
                
                [[self tableView] performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
            });
        }
        else {
            [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                           description:@"Errore durante l'ordinamento dei Wonder."
                                                                  type:TWMessageBarMessageTypeError callback:^{
                                                                      NSLog(@"Message bar tapped!");
                                                                  }];
            
        }
    }];
}

-(void)toggleButtons:(int)btn {
    [viewFollowerBtn setAlpha:0.3f];
    [viewMetaBtn setAlpha:0.3f];
    [viewWonderBtn setAlpha:0.3f];
    switch (btn) {
        case 1:
            [viewMetaBtn setAlpha:1.0f];
            break;
        case 0:
            [viewWonderBtn setAlpha:1.0f];
            break;
        case 2:
            [viewFollowerBtn setAlpha:1.0f];
            break;
        default:
            break;
    }
}

-(IBAction)doFollowOrUnfollow:(id)sender {
    if ([sender tag]) {
        //Unfollow
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[[PFUser currentUser] objectId], @"id",[currentViewControllerUser objectId], @"followId", nil];
        [PFCloud callFunctionInBackground:@"unfollowUser" withParameters:params block:^(id object, NSError *error) {
            if (!error) {
                [currentViewControllerUser fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                    [followersNumber setText:[[currentViewControllerUser numFollowers]stringValue]];
                }];
                [sender setTag:0];
                //[sender setBackgroundColor:[UIColor clearColor]];
                
                [sender setTitle:@"+ Follow" forState:UIControlStateNormal];
            }
            else {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                               description:error.localizedDescription
                                                                      type:TWMessageBarMessageTypeError callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
                
            }
        }];
    }
    else {
        //Do follow
        NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:[[PFUser currentUser] objectId], @"id",[currentViewControllerUser objectId], @"followId", nil];
        [PFCloud callFunctionInBackground:@"followUser" withParameters:params block:^(id object, NSError *error) {
            if (!error) {
                [currentViewControllerUser fetchInBackgroundWithBlock:^(PFObject *object, NSError *error) {
                    [followersNumber setText:[[currentViewControllerUser numFollowers]stringValue]];
                }];

                [sender setTag:1];
                [sender setTitle:@"- Followed" forState:UIControlStateNormal];
            }
            else {
                [[TWMessageBarManager sharedInstance] showMessageWithTitle:@"Errore!"
                                                               description:error.localizedDescription
                                                                      type:TWMessageBarMessageTypeError callback:^{
                                                                          NSLog(@"Message bar tapped!");
                                                                      }];
                
            }
        }];
        
    }
}

@end
