//
//  MainViewController.m
//  Wonderise
//
//  Created by Francesco D'Offizi on 17/01/14.
//
//

#import "MainViewController.h"
#import "TNAppDelegate.h"

#import "UnityIOSBridge.h"
@interface MainViewController ()

@end

@implementation MainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self enableBtns:NO];
    SlideNavigationController *snc = (SlideNavigationController*)[self navigationController];
    TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    [appDelegate setNavigationController:snc];

    /*[snc.navigationBar setBackgroundImage:[UIImage new]
                             forBarMetrics:UIBarMetricsDefault];
    snc.navigationBar.shadowImage = [UIImage new];
    snc.navigationBar.translucent = YES;
     */
    
    #warning linkEnable replace
    

}

- (void)createWalkthrough
{
    // Create the walkthrough view controller
    walkthrough = LAWalkthroughViewController.new;
    walkthrough.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    walkthrough.backgroundImage = [UIImage imageNamed:@"startuptutorial_border.png"];
    
    // Create pages of the walkthrough from XIBs
    [walkthrough addPageWithNibName:@"Intro" bundle:nil]; // no need for a view controller
    [walkthrough addPageWithNibName:@"Intro" bundle:nil]; // no need for a view controller
    [walkthrough addPageWithNibName:@"Intro" bundle:nil]; // no need for a view controller
    
    // Use text for the next button
    walkthrough.nextButtonText = @"Next >";
    
    UIButton *closeTutorial = [[UIButton alloc] init];
    [closeTutorial setTitle:@"Skip" forState:UIControlStateNormal];
    [closeTutorial setFrame:CGRectMake(10, self.view.frame.size.height-50, 40, 20)];

    [closeTutorial addTarget:self action:@selector(hideTutorial) forControlEvents:UIControlEventTouchUpInside];
    
    [[walkthrough view] addSubview:closeTutorial];
    
    // Add the walkthrough view to your view controller's view
    [self addChildViewController:walkthrough];
    [self.view addSubview:walkthrough.view];
}

-(void)hideTutorial {
    [walkthrough removeFromParentViewController];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
   // [self createWalkthrough];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)pushUnity:(id)sender {
    UnityIOSBridge *unityBridge = [[UnityIOSBridge alloc] init];
    [unityBridge loadCreationScene:@"{\"url\": \"http://files.parse.com/4f4614af-66b9-41e6-b21c-fc124f634960/0f76a995-7827-4e4a-a11a-58fc9b7cf275-ios.unity3d\"}"];
    TNAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    //[[self navigationController] pushViewController:[appDelegate unityVC] animated:YES];
    [[appDelegate unityVC] setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
    
    [self presentViewController:[appDelegate unityVC] animated:YES completion:^{
        NSLog(@"Ho Finito");
    }];
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
	return NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
    [self updateUserStatusBtns];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:animated];
    [super viewWillDisappear:animated];
}

- (void)enableBtns:(BOOL)enabled {
    float alphaLevel = 1.0f;
    
    if (!enabled) {
        alphaLevel = 0.5f;
    }

    [createBtn setEnabled:enabled];
    [profileBtn setEnabled:enabled];
    
    [createBtn setAlpha:alphaLevel];
    [profileBtn setAlpha:alphaLevel];
    
    [createLabel setAlpha:alphaLevel];
    [profileLabel setAlpha:alphaLevel];
    

}

-(void)updateUserStatusBtns {
    if ([PFUser currentUser]) {

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [loginSignupView setAlpha:0.0f];
        [logoutView setAlpha:1.0f];
        [UIView commitAnimations];
        [self hideLogoutButton];
        [loginBtn setEnabled:NO];
        [signupBtn setEnabled:NO];
        [logoutBtn setEnabled:NO];
        [self enableBtns:YES];
    }
    else {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [loginSignupView setAlpha:1.0f];
        [UIView commitAnimations];
        [logoutView setAlpha:0.0f];
        [loginBtn setEnabled:YES];
        [signupBtn setEnabled:YES];
        [logoutBtn setEnabled:NO];
        [self enableBtns:NO];
    }
}

- (IBAction)showLogoutButton:(id)sender {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [logoutView setFrame:CGRectMake(0, logoutView.frame.origin.y, logoutView.frame.size.width, logoutView.frame.size.height)];
    [logoutPostImg setAlpha:1.0f];
    [logoutBtn setEnabled:YES];
    [logoutArrow setEnabled:NO];
    [logoutArrow setAlpha:0.0f];
    [UIView commitAnimations];
}
- (void) hideLogoutButton {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    [logoutView setFrame:CGRectMake(-90, logoutView.frame.origin.y, logoutView.frame.size.width, logoutView.frame.size.height)];
    [logoutPostImg setAlpha:0.0f];
    [logoutBtn setEnabled:NO];
    [logoutArrow setEnabled:YES];
    [logoutArrow setAlpha:1.0f];
    [UIView commitAnimations];
}

- (void) viewDidLayoutSubviews {
    /* This set the scroller content width, fix problem when pushing-popping other view controllers */
    [scrollView setContentSize:CGSizeMake(633.0f, 250.0f)];
    [scrollView setScrollEnabled:YES];

}

- (IBAction)doLogout:(id)sender {
    [PFUser logOut];
    [self updateUserStatusBtns];
}
@end
