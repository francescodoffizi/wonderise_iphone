//
//  UnityIOSBridge.m
//  Unity-iPhone
//
//  Created by Francesco D'Offizi on 07/02/14.
//
//

#import "UnityIOSBridge.h"
void registerForRemoteNotifications()
{
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
}

void setWonderCreateSessionData(float posX, float posY, float posZ, float rotX, float rotY, float rotZ, float scale, char *path)
{
    NSString *messageFromUnity = [NSString stringWithUTF8String:path];
    NSLog(@"%@ %f",messageFromUnity, scale);
}

void closeUnityActivity(BOOL param) {
    if (param) {
        //ERROR
    }
    else {
        //ALLOK
        NSLog(@"ESCO!");
    }
}

void messageFromUnity(char *message)
{
    NSString *messageFromUnity = [NSString stringWithUTF8String:message];
    NSLog(@"%@",messageFromUnity);
}


@implementation UnityIOSBridge

-(void)loadCreationScene:(NSString *)dataToSend
{
    UnitySendMessage("WonderiseSceneManager", "LoadCreateScene", [dataToSend UTF8String]);
}

-(void)loadVisualizationScene:(NSString *)dataToSend
{
    UnitySendMessage("WonderiseSceneManager", "LoadViewScene", [dataToSend UTF8String]);
}



@end